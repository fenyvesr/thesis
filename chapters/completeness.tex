\chapter{Proof of Completeness}
\label{ch:complete}

Let us summarize what have we already achieved. So far we formally defined the four algebraic structures. We formally assessed their connections to each other by providing functors between their categories of algebras. We formally proved their equivalence by proving that the composition of the previously mentioned functors results in an isomorphism. The last topic in this thesis is to prove that one of the algebraic structure is complete.

To achieve the proof of completeness, we are using Stone's representation theorem, which introduces the subset algebra, ultrafilters and by using them, we can embed every boolean algebra into the subsets of its ultrafilters \cite{stone_1936}. These steps help to prove Gödel's completeness theory for classical propositional logic\cite{gödel_1930}.

\section{Syntax and semantics}

We have not talked about syntax and semantics yet. An algebra is an exact specification of the base set and the operations for which the axioms and inference rules are satisfied. Syntax is one particular algebra of the algebraic structure, it is also called initial algebra. Semantics are all the other algebras of the algebraic structure.

Maybe it is the easiest to demonstrate this on the algebraic structure of the Boolean algebra. The two valued and the subset algebras are both algebras for the same algebraic structure while their base sets and operations are fundamentally different. The two valued algebra uses $\{true,false\}$ as its basis, while the subset algebra uses the subsets of a particular set as its basis. Despite the difference in their semantics, both the two valued algebra and the subset algebra share common theorems. Those theorems which can be proven in the syntax hold true in all the semantics as well.

There is exactly one unique homomorphism from the syntax to each and every semantics. These homomorphisms are called recursors. They can be interpreted as an evaluation of the syntax or the compilation of the proofs in the semantics.

Gödel's completeness theorem for the classical propositional logic can be phrased the following way. If a formula is true in each subset algebra of the boolean algebra then the formula itself is true in the syntax.

\section{Subset algebra}

Subsets of a set $I$ form a boolean algebra, where the subsets of $I$ are the formulas. For a set $I$ we can specify $Subsets(I)$ the following way (see Code \ref{src:fam_mod})
\begin{align*}
	Form &:= I\rightarrow Prop\\
	\top&:= \lambda\ i\rightarrow\mathbb{1}\\
	\bot&:=\lambda\ i\rightarrow\mathbb{0}\\
	\neg A &:= \lambda\ i\rightarrow A\ i\rightarrow\mathbb{0}\\
	A \wedge B &:= \lambda\ i\rightarrow A\ i\times B\ i\\
	A\vee B&:=\lambda\ i\rightarrow A\ i+B\ i
\end{align*}
where $\mathbb{0}$ is the empty type, $\mathbb{1}$ is the unit type, $\times$ is the Certasian product type and $+$ is the coproduct type \cite{HoTT_2013}. A formula is a function, mapping the elements of $I$ to $Prop$, this can be interpreted as the characteristic function of a subset of $I$. If $A$ is a formula of $Subsets(I)$, then $A \ i$ is provable if and only if $i\in A$. Also we have to specify what the values of the propositional variables are, so we have to provide an $\iota : PV$ and $pv:PV\rightarrow I\rightarrow Prop$ for the $Subsets(I)$.

Given this algebra we can prove that $\wedge ass$, $\wedge com$, $\wedge id$, $\wedge dist$, $\wedge comp$, $\vee ass$, $\vee com$, $\vee id$, $\vee dist$ and $\vee comp$ are met (see Code \ref{src:prop_fam}).

\section{Ultrafilters}

Filters are an algebraic structure, which are meant to "filter" the formula set. This filtering happens along deduction. $top$ is always part of an filter, since everything implies $\top$. On the  other hand,$\bot$ is never part of a filter, since nothing implies $\bot$. If $A$ is part of an filter and $B$ can be deduced from $A$, then $B$ is also part of the filter, this is due to the modus ponens. If $A$ and $B$ is part of the filter then $A\wedge B$ is part of the filter as well, so it is closed to $\_\wedge\_$. A filter on algebra $M$ can be formally defined with the following algebraic structure (see Code \ref{src:fam_filt})
\begin{align*}
X&: Form^M\rightarrow Prop\\
\top^f&: X\ \top^M\\
\bot^f&:X\ \bot^M\rightarrow\mathbb{0}\\
\vdash^f&:X\ A\rightarrow A\vdash^M B\rightarrow X\ B\\
(X\ A)\wedge^f(X\ B) &: X\ (A\wedge^M B).
\end{align*} 
For any filters, the followings can be proven $X\ \top\equiv\mathbb{1}$, $X\ \bot\equiv\mathbb{0}$ and $X(A\wedge B)\equiv (X\ A)\times(X\ B)$.

Let us look at the proofs for these equalities. First, $X\ \top\equiv\mathbb{1}$ is provable, due to the propositional extensionality, which requires $X\ \top\rightarrow\mathbb{1}$ and $\mathbb{1}\rightarrow X\ \top$ to be provable. Both can be proved, the first with $*$, while the second with $\top^f$. All the other proofs are conducted the same way. By using the propositional extensionality it can be led back to implication.

We can also define filters which contain a specific $\varphi$ formula. This means, we can force one formula through the "filter". Of course it will bring all its consequences with it, since it is a filter.  A filter on algebra $M$ containing a specific $a$ formula can be formally defined with the following algebraic structure (see Code \ref{src:fam_filtphi})
\begin{align*}
	F&: Filter\ M\\
	X\varphi&: X^F\ \varphi
\end{align*} 

We can define another specific type of filter, called ultrafilter. An ultrafilter is specific in that way that for all formulas it is either containing the formula itself, or its negate. An ultrafilter on algebra $M$ can be formally defined with the following algebraic structure
\begin{align*}
F&: Filter\ M\\
lem^U&:X^F\ A+ X^F\ (\neg^M A).
\end{align*} 
For any ultrafilters, the followings can be proven, $X(\neg A)\equiv X\ A\rightarrow\mathbb{0}$ and $X(A\vee B)\equiv(X\ A)+(X\ B)$.

\section{Embedding}

After introducing $Subsets$ and $UltraFilter$, we are able to create a function, which maps every formula of a algebra $M$ to a formula of $Subsets(UltraFilter\ M)$. So there is an $f:Form^M\rightarrow Form^{Subsets(UltraFilter\ M)}$. We can easily prove that $f$ indeed defines a morphism from $M$ to $Subsets(UltraFilter\ M)$. We also need to prove that $f$ is injective. With this done the completeness can be proved in a few steps.

Let us define $f$ the following way $f:a\mapsto \lambda i \rightarrow X^i a$ (see Code \ref{src:fam_f}). This definition maps each formula onto a function, which takes an ultrafilter and gives back, whether $a$ is present in that ultrafilter or not. Since the parameter of the function is an ultrafilter, either $a$ or $\neg a$ is part of it, but not both.

We can easily prove that $f$ defines a morphism, by applying the functional extensionality and then using the already proven equalities of the ultrafilter (see Code \ref{src:fam_hom}).

The next step is to prove that $f$ is injective, so for any $a$ and $b$ formula of algebra $M$, if $f\ a\equiv f\ b$ then $a\equiv b$. To achieve this proof, first we prove a more stricter version of the injectivity. We prove that if $f\ a\equiv f\ \bot$ implies $a\equiv\bot$ and $f\ a\equiv f\ b$ is true, then $a\equiv b$. Why is it true? How can we step from $a\equiv\bot$, to $a\equiv b$?

The $reduce$ rule gives the following in a Boolean algebra, $(a\wedge\neg b)\vee(\neg a\wedge b)\equiv\bot$ then $a\equiv b$ (see $reduce$ at Code \ref{src:bool_uf}). By applying this, we have to prove $(a\wedge\neg b)\vee(\neg a\wedge b)\equiv\bot$ instead of $a\equiv b$. Why is it beneficial? Because, we know that if $f\ a\equiv f\ \bot$ is provable then $a\equiv\bot$ is provable as well. We can use this, so we have to prove, that
$f((a\wedge\neg b)\vee(\neg a\wedge b))\equiv f\ \bot$. Since $f$ defines a morphism, the equation can be transformed into the following with a few steps, $(f(a)\wedge\neg f(b))\vee(\neg f(a)\wedge f(b))\equiv f\ \bot$. Since $f\ a\equiv f\ b$, we can substitute into the equation, meaning we are left with $(f(a)\wedge\neg f(a))\vee(\neg f(a)\wedge f(a))\equiv f\ \bot$, which is true since $\wedge comp$ (see Code \ref{src:fam_injvar}). 

So we can prove the injectivity, if we can prove that, for any $a$ if $f\ a\equiv f\ \bot$ then $a\equiv\bot$. This means if none of the ultrafilters are containing $a$, then $a$ is $\bot$ itself. Let us prove it indirectly. So if $a\not\equiv\bot$, then $f\ a\not\equiv f\ \bot$, meaning that we can construct an ultrafilter which contains $a$. Let us create a chain of filters containing $a$. It means that we define a partial ordering between the filters containing $a$. The partial ordering $X_n\subset X_m$ can be defined the following way, for any formula $B$, if $X_n\ B$ is true then $X_m\ B$ is also true (see Code \ref{src:fam_sub}). In case of chains, we require that for any $X_0$, $X_1$, $X_2$, $\dots$ elements of the chain $X_i\subset X_{i+1}$ (see Code \ref{src:fam_chain}). So if a formula is somehow becomes the element of a chain, then it stays in it forever.

We can also define the greatest element of a chain. The greatest element of a chain comparable with every element of the chain and it is greater than any of them (see Code \ref{src:fam_grt}).

If every chain of filters containing $a$ have a greatest element, then there is a maximal element for all filters containing $a$ provided by the Zorn-lemma. We can prove that this maximal element is a filter and an ultrafilter at the same time. This maximal element is needed to prove that $f\ a\neq f\ \bot$.

But why has every chain of filters containing $a$ have a greatest element. Let us propose the union of all the filters in the chain as a greatest element (see Code \ref{src:fam_lemma}).
$\top$ is part of this greatest element, since it is part of every filter in the chain, so it is part of the union as well. $\bot$ is not part of the greatest element, since it is not part of any of the filters in the chain. If $A$ is element of the filter and $A\equiv A\wedge B$, then there exists an $X^n$ for which $X^n\ A$ is true, but then $X^n\ B$ is true as well, so $B$ is in the greatest element as well. If $A$ and $B$ are in the greatest element then there exist an $X^n$ and $X^m$ for which $X^n\ A$ and $X^m\ B$ are true. We can take the bigger value, let it be $n$ without limiting the generality of the foregoing, so $X^n\ B$ is true as well, since $X^m\subset\dots\subset X^n$ holds for the chain. Since $X^n\ A$ and $X^n\ B$ we know that $X^n (A\wedge B)$, which means $A\wedge B$ is part of the greatest element. $a$ is also in the greatest element since it is element of all the filters in the chain.

So every chain containing $a$ has a greatest element. The Zorn-lemma gives a maximal element for the filters containing $a$ (see Code \ref{src:fam_zorn}). Let us prove that it is an ultrafilter. We only have to prove that $lem$ is true in the maximal element.

Let us suppose that $lem$ is not true, so there is a $B$ for which $X\ B\rightarrow\mathbb{0}$ and $X(\neg B)\rightarrow\mathbb{0}$. Let us create a set which is given by the following function $X\ b\mapsto \exists u\in Form^M:X\ u\times ((u\wedge B)\vdash b)$, where $X$ is the filtering function of the maximal element.

We can prove that the set determined by this function is a filter containing $a$ (see Code \ref{src:fam_maxlem}). $\top$ is in it, since $\top$ is a formula for which $X\ \top$ is true and $\top\wedge B\vdash \top$. $\bot$ is not an element of this set since if there exists $u$ for which $X\ u$ and $(u\wedge B)\vdash\bot$ holds, then we would know that $u\vdash\neg B$ is true (see $neg$ at Code \ref{src:bool_uf}), so $X\ \neg B$ is true as well, but that is contradicting the initial assumption that $\neg B$ is not part of $X$. If $A$ is in it and $A\vdash b$, then it means that there exists an $u$ formula for which $(u\wedge B)\vdash A$. From this we can see that this $u$ is suitable for $b$ as well, since the principle of syllogism, $\vdash$ is transitive, so $(u\wedge B)\vdash b$ holds as well. If $A$ and $b$ are in the set, then there exists a $u_A$ and a $u_b$, from which we can use $u_A\wedge u_b$, to prove that $((u_A\wedge u_b)\wedge B)\vdash (A\wedge b)$ holds. This set also contains $a$ since $a$ is a good formula for which $X\ a$ is true and $(a\wedge B)\vdash a$ is also true. The set also contains $B$ since $(\top\wedge B)\vdash B$ and $X\ \top$.

So we found a filter containing $a$ and $B$, so it is greater than the maximal element which is a contradiction so either $B$ or $\neg B$ is part of the maximal element, making it not just a filter but an ultrafilter.

Since we found an ultrafilter containing $a$ we can see that $f\ a\not\equiv f\ \bot\equiv\mathbb{0}$ (see Code \ref{src:fam_inj}).

\begin{theorem}[Completeness]
	Let $S$ be the syntax of the Boolean algebra. Then for any set $I$, there is a unique recursor $\llbracket\_\rrbracket$ from the syntax to the subset algebra of $I$. If for any set $I$, a formula's value by the recursor is equal to $\top$ in the subset algebra, then the formula is equal to top in the syntax.
	
	Formally this can be written in the follwoing way. For any $I$, let $\llbracket\_\rrbracket:Form^S\rightarrow Form^{Subsets(I)}$ be the recursor. If for any $I$ $\llbracket\varphi\rrbracket\equiv\top^{Subsets(I)}$, then $\varphi\equiv\top^S$.
\end{theorem}
\begin{proof}
	Since for any set $I$ we know that $\llbracket\varphi\rrbracket\equiv\top^{Subsets(I)}$ holds, thus it is true for $I=UltraFilter\ S$ as well. So we have to prove that if $\llbracket\varphi\rrbracket\equiv\top^{Subsets(Ultrafilter\ S)}=f\ \top$ then $\varphi\equiv\top^S$. However this is the already proven injectivity property of $f$. So since $f$ is injective, $\llbracket\varphi\rrbracket\equiv f\ \top$ implies $\varphi\equiv\top^S$ (see Code \ref{src:fam_compl}).
\end{proof}