\section{Nicod's system}
\label{sec:alg_nic}

Knowing the informal meaning of the logical connectives, we can notice that there is redundancy between them. $\_\wedge\_$ and $\_\vee\_$ can be expressed by $\_\Rightarrow\_$ and $\neg$. For example, $p\wedge q$ means the same as $\neg(p\Rightarrow\neg q)$ and $p\vee q$ means the same as $\neg p\Rightarrow q$. We can create a real life demonstration for the second case. If I know that either it is raining or the sun is shining, then if it is not raining then the sun is shining. And it can be turned around. If I know that if it is not raining then the sun shines, then it's either raining or the sun is shining.

What is the minimal number of logical connectives needed to express all the other operations. Sheffer found that all the connectives can be expressed by one connective \cite{sheffer_1913}. We use the Sheffer's stroke ($\_\uparrow\_$) symbol to denote the logical \textit{nand} operation. $p\uparrow q$ is false only if $p$ and $q$ are both true. The other connectives can be expressed by \textit{nand} the following way (see Code \ref{src:nic_neg},\ref{src:nic_Rightarrow},\ref{src:nic_vee},\ref{src:nic_wedge})
\begin{align*}
	\neg p&:=p\uparrow p&
	p\Rightarrow q&:=p\uparrow(q\uparrow q)\\
	p\wedge q&:=(p\uparrow q)\uparrow(p\uparrow q)&
	p\vee q&:=(p\uparrow p)\uparrow(q\uparrow q).
\end{align*}
We may notice that Sheffer's stroke brings the four logical connectives closer. The following formulas are equal, while philosophically totally different.
$$((p \uparrow p) \uparrow (p \uparrow p)) \uparrow (p \uparrow p) \equiv p \vee p \Rightarrow p \equiv \neg p \vee p \equiv \neg \neg p \Rightarrow p$$
Let us look at the last two formulas. "It is either not raining or raining" statement seems to have no connection to the "if it is not not raining then it is raining" statement, and still expressing them with Sheffer's stroke we end up with the same formulas.

Peirce also found that \textit{nor} has the same property, he introduced the Peirce's arrow ($\downarrow$) notation for the \textit{nor} connective \cite{büning_hans_kleine_lettmann_1999}. $p\downarrow q$ is true only if $p$ and $q$ are both false. The other connectives can be expressed by \textit{nor} the following way
\begin{align*}
	\neg p&:=p\downarrow p&
	p\Rightarrow q&:=((p\downarrow p)\downarrow q)\downarrow((p\downarrow p)\downarrow q)\\
	p\wedge q&:=(p\downarrow p)\downarrow(q\downarrow q)&
	p\vee q&:=(p\downarrow q)\downarrow(p\downarrow q).
\end{align*}

From expressing the other connectives, it can be seen, that it is easier to work with $\_\uparrow\_$ than with $\_\downarrow\_$. It is shorter to express tautologies with Sheffer's stroke than with Peirce's arrow.
$$
p\uparrow(p\uparrow p)\equiv p\Rightarrow p\equiv((p\downarrow p)\downarrow p)\downarrow((p\downarrow p)\downarrow p)
$$
This example can indicate that tautologies expressed with $\_\downarrow\_$ are generally longer than expressing them with $\_\uparrow\_$.

After founding the minimal number of logical connectives needed to express all the other logical connectives, we can also ask ourselves what is the minimal number of axioms needed to describe the classical propositional logic. Based on Sheffer's work Nicod found that only one axiom is needed \cite{nicod_1920}.
Later \L ukasiewicz found more closely related axioms to Nicod's axiom \cite{pogorzelski_lukasiewicz_slupecki_wydawnictwo_1965}.

\subsection{Informal description of Nicod's system}

We can describe classical propositional logic as the following system, $\Omega = \Omega_2 = \{\_\uparrow\_\}$, $Z = \{ rule\}$ and $I = \{prop\}$, where $rule$ and $prop$ are
\begin{prooftree}
	\AxiomC{$\vdash (p \uparrow (r \uparrow q))$}
	\AxiomC{$\vdash p$}
	\LeftLabel{rule}
	\BinaryInfC{$\vdash q$}
\end{prooftree}
\begin{prooftree}
	\AxiomC{}
	\LeftLabel{prop$(p,q,r,s,t)$}
	\UnaryInfC{$\vdash((p\uparrow(q\uparrow r))\uparrow((t\uparrow(t\uparrow t))\uparrow((s\uparrow q)\uparrow((p\uparrow s)\uparrow(p\uparrow s)))))$}.
\end{prooftree}
The $\vdash$ sign is called "assertion-sign". It means that what follows is asserted \cite{russell_whitehead_1935}. We have to distinguish between a proposition with or without the assertion-sign. A proposition with assertion-sign refers to the proof trees which prove the specific formula. A proposition without assertion-sign is just a proposition, not yet evaluated. 

$rule$ is also called Nicod's modus ponens. It is not the usual inference rule where "if $p$ is asserted and $p\Rightarrow q$ is asserted, then $q$ is asserted". This usual inference rule in Nicod's system would look something like "if $p$ is asserted and $p\uparrow(q\uparrow q)$ is asserted, then $q$ is asserted". Unlike this, Nicod's modus ponens has a third proposition $r$ in it, not just $p$ and $q$. This modification is crucial for the calculus \cite{scharle_1965}. During our work with Nicod's logical system we follow his paper "A Reduction in the number of the Primitive Propositions of Logic" \cite{nicod_1920}.

Let us dive into the system described above. First we prove some basic theorems by hand and later we rely on proofs implemented in Agda (see Appendix \ref{appx:nicod_proofs}).

\subsection{Proofs in Nicod's system}

\begin{theorem}[nic-lem(s,t,u)]
	Let $\pi = t\uparrow(t\uparrow t)$, where $t\in \mathcal{L}$. For any $s,u \in \mathcal{L}$, we can prove that $\vdash((u\uparrow(\pi\uparrow s))\Rightarrow ((s\uparrow \pi)\uparrow u))$.
\end{theorem}
\begin{proof}
	Let $t,s,u\in\mathcal{L}$, $\pi = t\uparrow(t\uparrow t)$, $Q_1=(s\uparrow t)\Rightarrow(t\uparrow s)$ and $Q_2=(u\uparrow(\pi\uparrow s))\Rightarrow ((s\uparrow \pi)\uparrow u)$.
	\begin{align*}
		1.& \vdash(((s\uparrow\pi)\Rightarrow(\pi\uparrow s))\uparrow(\pi\uparrow Q_2))&&
		[prop(s\uparrow\pi,\pi\uparrow s,\pi\uparrow s,u,t)]\\
		2.& \vdash((\pi\uparrow(\pi\uparrow Q_1))\uparrow(\pi\uparrow((s\uparrow\pi)\Rightarrow(\pi\uparrow s))))&&
		[prop(\pi,\pi,Q_1,s,t)]\\
		3.& \vdash (\pi\uparrow(\pi\uparrow Q_1))&&
		[prop(t,t,t,s,t)]\\
		4.& \vdash((s\uparrow\pi)\Rightarrow(\pi\uparrow s))&&
		[rule(2,3)]\\
		5.& \vdash((u\uparrow(\pi\uparrow s))\Rightarrow((s\uparrow \pi)\uparrow u))&&
		[rule(1,4)]
	\end{align*}

	For implementation see Code \ref{src:nic_lem}.
\end{proof}

After the first proof, we can prove the principle of identity, which states that any proposition implies itself \cite{russell_whitehead_1935}. Formally, for any $t\in\mathcal{L}$ in Nicod's system $\vdash t\uparrow (t\uparrow t)$ can be deduced.

\begin{theorem}
\label{th:nic_tau}
	For any $t \in \mathcal{L}$, we can prove that $\vdash(t\Rightarrow t)$.
\end{theorem}
\begin{proof}
	Let $t\in\mathcal{L}$, $\pi=t\Rightarrow t$, $Q_1=(\pi\uparrow t)\Rightarrow(t\uparrow \pi)$, $T=\pi\uparrow(\pi\uparrow Q_1)$, $A=(Q_1\uparrow\pi)\uparrow\pi$ and $B=(\pi\uparrow t)\Rightarrow A$.
	\begin{align*}
		1.&\vdash((T\Rightarrow A)\uparrow(\pi\uparrow(((B\uparrow\pi)\uparrow A)\Rightarrow(T\uparrow(B\uparrow\pi)))))&&
		[prop(T,A,A,B\uparrow\pi,t)]\\
		2.&\vdash(T\Rightarrow A)&&
		[\mathrm{nic-lem}(Q_1,t,\pi)]\\	
		3.&\vdash(((B\uparrow\pi)\uparrow A)\Rightarrow(T\uparrow(B\uparrow\pi)))&&
		[rule(1,2)]\\	
		4.& \vdash((A\uparrow(\pi\uparrow B))\Rightarrow ((B\uparrow \pi)\uparrow A))&&
		[\mathrm{nic-lem}(B,t,A)]\\
		5.& \vdash(A\uparrow(\pi\uparrow B))&&
		[prop(Q_1\uparrow\pi,t,t\uparrow t,\pi,t)]\\
		6.& \vdash((B\uparrow \pi)\uparrow A)&&
		[rule(4,5)]\\
		7.& \vdash(T\uparrow(B\uparrow\pi))&&
		[rule(3,6)]\\
		8.& \vdash T&&
		[prop(t,t,t,\pi,t)]\\
		9.& \vdash\pi&&
		[rule(7,8)]
	\end{align*}

	For implementation see Code \ref{src:nic_id}.
\end{proof}

We can see that even this minimal logical system is capable of providing proofs, but a lot of tricks are needed to eliminate parts of $prop$ to end up with the formula needed. Next we prove that $\_\uparrow\_$ is commutative.

\begin{theorem}
	For any $p,s \in \mathcal{L}$, we can prove that $\vdash((s\uparrow p)\Rightarrow(p\uparrow s))$.
\end{theorem} 
\begin{proof}
	See Code \ref{src:nic_swap}.
\end{proof}

The principle of simplification can be also proved \cite{russell_whitehead_1935}. This allows us to move from a joint assertion of $p$ and $s$ to the assertion of only $s$.

\begin{theorem}[nic-add]
	\label{th:nic-add}
	For any $p,s \in \mathcal{L}$, we can prove that $\vdash(s\Rightarrow(p\Rightarrow s))$.
\end{theorem} 
\begin{proof}
	See Code \ref{src:nic_add}.
\end{proof}

The next step is to prove the principle of syllogism, for this we need to prove a similar statement first \cite{russell_whitehead_1935}.

\begin{theorem}
	\label{th:nik-luk1}
	For any $p,q,r,s \in \mathcal{L}$, we can prove that $\vdash((p\uparrow(q\uparrow r))\Rightarrow((q\uparrow s)\Rightarrow(p\uparrow s)))$.
\end{theorem} 
\begin{proof}
	See Code \ref{src:nic_luk1}.
\end{proof}

From Theorem \ref{th:nik-luk1} we can prove $\vdash(p\Rightarrow q)\Rightarrow((q\Rightarrow s)\Rightarrow(p\Rightarrow s))$ by writing $q$ instead of $r$ and $s\uparrow s$ instead of $s$. This yields the principle of syllogism. With the help of Theorem \ref{th:nik-luk1} we are able to build long proof chains. It provides the transitive property for $\_\Rightarrow\_$.

The last proof is the commutative principle \cite{russell_whitehead_1935}.

\begin{theorem}
	\label{th:nic-ass}
	For any $p,q,r\in \mathcal{L}$, we can prove that $\vdash((p\Rightarrow(q\uparrow r))\Rightarrow(q\Rightarrow(p\uparrow r)))$.
\end{theorem} 
\begin{proof}
	See Code \ref{src:nic_ass}.
\end{proof} 

From Theorem \ref{th:nic-ass} we can prove $\vdash((p\Rightarrow (q\Rightarrow r))\Rightarrow(q\Rightarrow(p\Rightarrow r)))$ by writing $r\uparrow r$ instead of $r$. Based on these theorems we can lay the foundation for the other descriptions of classical propositional logic. For further proofs see Code \ref{src:nic_proofs}.

\subsection{Conclusion about Nicod's system}

After working with Nicod's system, we can conclude that writing proofs in it is not convenient. The meaning of the formulas in the language is intertwined with the encoding of the formulas. Proving a formula true is not based on its meaning rather it is based on tricks founded deep in the specification. Let us inspect other systems describing propositional logic in the following sections. These have more and more axioms and inference rules as we move forward. With more and more tools in our hand, the structure of the proof trees proving a formula is easier to comprehend since it gets closer and closer to the natural way of human thinking.