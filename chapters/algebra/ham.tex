\section{Hamilton's system}
\label{sec:alg_ham}

The next system contains three axioms which can be found at various places in the literature \cite{frege_1879,margaris_1967,russell_whitehead_1935}. We call them Hamilton's system based on the book "Logic for mathematicians" \cite{hamilton_1978}.

\subsection{Informal description of Hamilton's system}

We can describe classical propositional logic as the following system, $\Omega =\Omega_1\cup \Omega_2 = \{\neg\}\cup\{\_\Rightarrow\_\}$, $Z = \{ \_\$\_ \}$ and $I = \{L_1,L_2,L_3\}$, where $\_\$\_$, $L_1$,$L_2$ and $L_3$ are the followings.
\begin{prooftree}
	\AxiomC{$\vdash (\varphi\Rightarrow\psi)$}
	\AxiomC{$\vdash \varphi$}
	\LeftLabel{$\_\$\_$}
	\BinaryInfC{$\vdash \psi$}
\end{prooftree}
\begin{prooftree}
	\AxiomC{}
	\LeftLabel{$L_1(\varphi,\psi)$}
	\UnaryInfC{$\vdash(\varphi\Rightarrow(\psi\Rightarrow\varphi))$}
\end{prooftree}
\begin{prooftree}
	\AxiomC{}
	\LeftLabel{$L_2(\varphi,\psi,\chi)$}
	\UnaryInfC{$\vdash((\varphi\Rightarrow(\psi\Rightarrow\chi))\Rightarrow((\varphi\Rightarrow\psi)\Rightarrow(\varphi\Rightarrow\chi)))$}
\end{prooftree}
\begin{prooftree}
	\AxiomC{}
	\LeftLabel{$L_3(\varphi,\psi)$}
	\UnaryInfC{$\vdash(((\neg\varphi)\Rightarrow(\neg\psi))\Rightarrow(\psi\Rightarrow\varphi))$}
\end{prooftree}

$\vdash$ means the same as in the previous section. $L_1$ is the principle of simplification which is discussed earlier at the Nicod's system (see Code \ref{src:nic_add}). $L_2$ is part of Frege's original system \cite{frege_1879}, which can be interpreted as it distributes an antecedent over two succedents. $L_3$ is the principle of transposition, which reverses the order of the formulas when negation is eliminated.

The other connectives can be expressed by $\_\Rightarrow\_$ and $\neg$ the following way (see Code \ref{src:ham_vee},\ref{src:ham_wedge},\ref{src:ham_nand})
\begin{align*}
	\varphi\wedge\psi&:=\neg(\varphi\Rightarrow(\neg \psi))&
	\varphi\vee\psi&:=(\neg\varphi)\Rightarrow\psi&
	\varphi\uparrow\psi&:=\varphi\Rightarrow(\neg \psi).
\end{align*}

Let us dive into the system described above. First we prove some basic theorems by hand and later we rely on proofs implemented in Agda (see Appendix \ref{appx:ham_proofs}).

\subsection{Proofs in Hamilton's system}

First we can prove again the principle of identity, which states that any proposition implies itself. More formally, for any $\varphi\in\mathcal{L}$ in Hamilton's system $\varphi\Rightarrow\varphi$ is true.

\begin{theorem}
	\label{th:ham_id}
	For any $\varphi\in \mathcal{L}$, we can prove that $\vdash(\varphi\Rightarrow\varphi)$.
\end{theorem}
\begin{proof}
	Let $\varphi\in\mathcal{L}$.
	\begin{align*}
		1.&\vdash((\varphi\Rightarrow((\varphi\Rightarrow\varphi)\Rightarrow\varphi))\Rightarrow((\varphi\Rightarrow(\varphi\Rightarrow\varphi))\Rightarrow(\varphi\Rightarrow\varphi)))&&
		[L_2]\\
		2.& \vdash(\varphi\Rightarrow((\varphi\Rightarrow\varphi)\Rightarrow\varphi))&&
		[L_1]\\
		3.& \vdash((\varphi\Rightarrow(\varphi\Rightarrow\varphi))\Rightarrow(\varphi\Rightarrow\varphi))&&
		[1\$2]\\
		4.& \vdash(\varphi\Rightarrow(\varphi\Rightarrow\varphi))&&
		[L_1]\\
		5.& \vdash(\varphi\Rightarrow\varphi)&&
		[3\$4]
	\end{align*}
	
	For implementation see Code \ref{src:ham_id}.
\end{proof}

After the first proof, we prove the principle of syllogism \cite{russell_whitehead_1935}. We already mentioned that this principle helps building proofs by enabling us to create proof chains. It unlocks the transitive property of the implication.

\begin{theorem}
	For any $\varphi,\psi,\chi\in \mathcal{L}$, we can prove that if $\vdash(\psi\Rightarrow\chi)$ and $\vdash(\varphi\Rightarrow\psi)$ are provided then $\vdash(\varphi\Rightarrow\chi)$ can be proven.
\end{theorem}
\begin{proof}
	Let $\varphi,\psi, \chi\in \mathcal{L}$. Let us assume that $\vdash(\psi\Rightarrow\chi)$ and $\vdash(\varphi\Rightarrow\psi)$ are already proven.
	\begin{align*}
		1.&\vdash((\varphi\Rightarrow(\psi\Rightarrow\chi))\Rightarrow((\varphi\Rightarrow\psi)\Rightarrow(\varphi\Rightarrow\chi)))&&
		[L_2]\\
		2.& \vdash((\psi\Rightarrow\chi)\Rightarrow(\varphi\Rightarrow(\psi\Rightarrow\chi)))&&
		[L_1]\\
		3.& \vdash(\psi\Rightarrow\chi)&&
		[hyp1]\\
		4.& \vdash(\varphi\Rightarrow(\psi\Rightarrow\chi))&&
		[2\$3]\\
		5.& \vdash((\varphi\Rightarrow\psi)\Rightarrow(\varphi\Rightarrow\chi))&&
		[1\$4]\\
		6.& \vdash(\varphi\Rightarrow\psi)&&
		[hyp2]\\
		7.& \vdash(\varphi\Rightarrow\chi)&&
		[5\$6]
	\end{align*}
	
	For implementation see Code \ref{src:ham_syl}.
\end{proof}

After these two proofs, we can already see that creating proofs in Hamilton's system is closer to the human intuition than it is in Nicod's system. We can see that more and more inference rules can be deduced from the system. This is the main difference between Nicod's and Hamilton's system. Providing proofs in Nicod's system is more about the structure of the formula, unlike in Hamilton's system, where proofs are more about the meaning of the formula. The last proof is the commutative principle \cite{russell_whitehead_1935}.
\begin{theorem}
	For any $\varphi,\psi,\chi\in \mathcal{L}$, we can prove that if $\vdash(\varphi\Rightarrow(\psi\Rightarrow\chi))$ then $\vdash(\psi\Rightarrow(\varphi\Rightarrow\chi))$.
\end{theorem}
\begin{proof}
	Let $\varphi,\psi, \chi\in \mathcal{L}$ and  $\vdash(\varphi\Rightarrow(\psi\Rightarrow\chi))$.
	\begin{align*}
		1.&\vdash((\varphi\Rightarrow(\psi\Rightarrow\chi))\Rightarrow((\varphi\Rightarrow\psi)\Rightarrow(\varphi\Rightarrow\chi)))&&
		[L_2]\\
		2.&\vdash(\varphi\Rightarrow(\psi\Rightarrow\chi))&&
		[hyp]\\
		3.& \vdash((\varphi\Rightarrow\psi)\Rightarrow(\varphi\Rightarrow\chi))&&
		[1\$2]\\
		4.& \vdash(\psi\Rightarrow(\varphi\Rightarrow\psi)) &&
		[L_1]\\
		5.& \vdash(\psi\Rightarrow(\varphi\Rightarrow\chi))&&
		[syl(3,4)]
	\end{align*}
	
	For implementation see Code \ref{src:ham_com12}.
\end{proof}

Based on these theorems we can lay the foundation for the other descriptions of classical propositional logic. For further proofs see Code \ref{src:ham_proofs}.

\subsection{Conclusion about Hamilton's system}

Instead of using a logical connective which is hard to understand, Hamilton's system is using implication and negation as its base connectives. "If $p$ then $q$" and "not $p$" are easier to comprehend than "not($p$ and $q$)". This way, Hamilton's system is closer to the human intuition, which makes working with it much easier. Proofs are not so hard to provide. Overall Hamilton's system is more user friendly than Nicod's system.