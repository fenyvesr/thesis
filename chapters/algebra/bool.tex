\section{Boolean system}

Last but not least, let us investigate a system which is somewhat different from the already presented ones. In the Boolean system, we do not use the so far used assertion-sign. Instead, Boolean system works with the equality of formulas by introducing equality rules. We can sense that those formulas which are equal to the logical constant true in the Boolean algebra are the ones which can be asserted in the previous systems. 

\subsection{Informal description of the Boolean system}

We can describe classical propositional logic as the following system, $\Omega=\Omega_0\cup\Omega_1\cup\Omega_2=\{\top,\bot\}\cup\{\neg\}\cup\{\wedge,\vee\}$, $Z = \{ \_^{-1},\_\sq\_,ap \}$ and $I = \{\wedge ass,\wedge com,\wedge id,\wedge dist,\wedge comp,\vee ass,\vee com,\vee id,\vee dist,\vee comp\}$, where the terms are the followings.
\begin{center}
	\AxiomC{$a\equiv b$}
	\LeftLabel{$\_^{-1}$}
	\UnaryInfC{$b\equiv a$}
	\DisplayProof
	\quad
	\AxiomC{$a\equiv b$}
	\AxiomC{$b\equiv c$}
	\LeftLabel{$\_\sq\_$}
	\BinaryInfC{$a\equiv c$}
	\DisplayProof
	\quad
	\AxiomC{$f:\mathcal{L}\rightarrow\mathcal{L}$}
	\AxiomC{$a\equiv b$}
	\LeftLabel{$ap$}
	\BinaryInfC{$f(a)\equiv f(b)$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{$\wedge com$}
	\UnaryInfC{$a\wedge b\equiv b\wedge a$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\wedge id$}
	\UnaryInfC{$a\wedge\top\equiv a$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\wedge comp$}
	\UnaryInfC{$a\wedge\neg a\equiv\bot$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{$\wedge ass$}
	\UnaryInfC{$a\wedge(b\wedge c)\equiv(a\wedge b)\wedge c$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\wedge dist$}
	\UnaryInfC{$a\wedge(b\vee c)\equiv(a\wedge b)\vee(a\wedge c)$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{$\vee com$}
	\UnaryInfC{$a\vee b\equiv b\vee a$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\vee id$}
	\UnaryInfC{$a\vee\bot\equiv a$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\vee comp$}
	\UnaryInfC{$a\vee\neg a\equiv\top$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{$\vee ass$}
	\UnaryInfC{$a\vee(b\vee c)\equiv(a\vee b)\vee c$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\vee dist$}
	\UnaryInfC{$a\vee(b\wedge c)\equiv(a\vee b)\wedge(a\vee c)$}
	\DisplayProof
\end{center}

We can see that this system is described with ten axioms. As we already discussed at Nicod's system, fewer axioms can be also enough. Stephen Wolfram found that only one axiom is necessary for describing a similar system. $((a\uparrow b)\uparrow c)\uparrow(a\uparrow((a\uparrow c)\uparrow a))\equiv c$ rule is one of his suggestions \cite{wolfram_2019}.

The inference rules are the properties of the $\_\equiv\_$. Since $\_\equiv\_$ is an equality relation, it is symmetric, transitive and functions respect equality.

Let us dive into the system described above. We prove some basic theorems first by hand and later we rely on proofs implemented in Agda (see Appendix \ref{appx:bool_proofs}).

\subsection{Proofs in Boolean system}

First, let us prove that the $\_\wedge\_$ distributes over $\_\vee\_$ from the right side as well. $\wedge dist$ only states that $\_\wedge\_$ distributes over $\_\vee\_$ from the left side.

\begin{theorem}
	For any $a,b,c\in \mathcal{L}$, we can prove that $(b\wedge a)\vee (c\wedge a)\equiv(b\vee c)\wedge a$.
\end{theorem}
\begin{proof}
	Let $a,b,c\in \mathcal{L}$. For making the proof more readable, let us introduce $\varphi_0 := (b\wedge a)\vee (c\wedge a)$, $\varphi_1:=(a\wedge b)\vee (c\wedge a)$, $\varphi_2:=(a\wedge b)\vee (a\wedge c)$, $\varphi_3:=a\wedge (b\vee c)$ and $\varphi_4:=(b\vee c)\wedge a$. We can prove the equality of some of these formulas.
	
	\begin{center}
		\AxiomC{$\chi\mapsto  \chi\vee(c\wedge a)$}
		\AxiomC{}
		\LeftLabel{$\wedge com$}
		\UnaryInfC{$b\wedge a\equiv a\wedge b$}
		\LeftLabel{$ap$}
		\BinaryInfC{$\varphi_0=(b\wedge a)\vee (c\wedge a)\equiv(a\wedge b)\vee (c\wedge a)=\varphi_1$}
		\DisplayProof
	\end{center}
	\begin{center} 
		\AxiomC{$\chi\mapsto  (a\wedge b)\vee\chi$}
		\AxiomC{}
		\LeftLabel{$\wedge com$}
		\UnaryInfC{$c\wedge a\equiv a\wedge c$}
		\LeftLabel{$ap$}
		\BinaryInfC{$\varphi_1=(a\wedge b)\vee (c\wedge a)\equiv(a\wedge b)\vee (a\wedge c)=\varphi_2$}
		\DisplayProof
	\end{center}
	After proving the equality of the above mentioned formulas, we can put the proof together.
	\begin{center}
		\def\defaultHypSeparation{\hskip .5in}
		\AxiomC{$\varphi_0\equiv \varphi_1$}
		\AxiomC{$\varphi_1\equiv \varphi_2$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\varphi_0\equiv \varphi_2$}
		\AxiomC{}
		\LeftLabel{$\wedge dist$}
		\UnaryInfC{$\varphi_3\equiv \varphi_2$}
		\LeftLabel{$\_^{-1}$}
		\UnaryInfC{$\varphi_2\equiv \varphi_3$}
		\AxiomC{}
		\LeftLabel{$\wedge com$}
		\UnaryInfC{$\varphi_3\equiv \varphi_4$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\varphi_2\equiv \varphi_4$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$(b\wedge a)\vee (c\wedge a)\equiv(b\vee c)\wedge a$}
		\DisplayProof
	\end{center}
	For implementation see Code \ref{src:bool_andrdist}.
\end{proof}

The same can be proved about $\_\vee\_$. Only the corresponding rules of $\_\vee\_$ has to be used instead. For implementation see Code \ref{src:bool_orrdist}. As a second proof, we prove that $\top$ is a right zero element of $\_\vee\_$. It means that any formula connected to $\top$ with $\_\vee\_$ from the right equals $\top$.

\begin{theorem}
	For any $a\in \mathcal{L}$, we can prove that $a\vee\top\equiv \top$.
\end{theorem}
\begin{proof}
	Let $a\in \mathcal{L}$. For making the proof more readable, let us introduce
	$\varphi_0:= a\vee\top$,
	$\varphi_1:=(a\vee\top)\wedge\top$,
	$\varphi_2:=(a\vee\top)\wedge(a\vee\neg a)$,
	$\varphi_3:=a\vee(\top\wedge\neg a)$,
	$\varphi_4:=a\vee\neg a$ and $\varphi_5:=\top$. First let us prove the equality of some of these formulas.
	
	\begin{center}
		\AxiomC{$\chi\mapsto(a\vee\top)\wedge\chi$}
		\AxiomC{}
		\LeftLabel{$\vee comp$}
		\UnaryInfC{$a\vee\neg a\equiv\top$}
		\LeftLabel{$\_^{-1}$}
		\UnaryInfC{$\top\equiv a\vee\neg a$}
		\LeftLabel{$ap$}
		\BinaryInfC{$\varphi_1=(a\vee\top)\wedge\top\equiv(a\vee\top)\wedge(a\vee\neg a)=\varphi_2$}
		\DisplayProof
	\end{center}
	\begin{center}
		\AxiomC{$\chi\mapsto  a\vee\chi$}
		\AxiomC{}
		\LeftLabel{$\wedge com$}
		\UnaryInfC{$\top\wedge\neg a\equiv \neg a\wedge\top$}
		\AxiomC{}
		\LeftLabel{$\wedge id$}
		\UnaryInfC{$\neg a\wedge\top\equiv\neg a$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\top\wedge\neg a\equiv\neg a$}
		\LeftLabel{$ap$}
		\BinaryInfC{$\varphi_3=a\vee(\top\wedge\neg a)\equiv a\vee\neg a=\varphi_4$}
		\DisplayProof
	\end{center}
	After proving the equality of the above mentioned formulas, we can put the proof together.
	\begin{center}
		\def\defaultHypSeparation{\hskip .1in}
		\AxiomC{}
		\LeftLabel{$\wedge id$}
		\UnaryInfC{$\varphi_1\equiv\varphi_0$}
		\LeftLabel{$\_^{-1}$}
		\UnaryInfC{$\varphi_0\equiv \varphi_1$}
		\AxiomC{$\varphi_1\equiv \varphi_2$}
		\AxiomC{}
		\LeftLabel{$\vee dist$}
		\UnaryInfC{$\varphi_3\equiv\varphi_2$}
		\LeftLabel{$\_^{-1}$}
		\UnaryInfC{$\varphi_2\equiv \varphi_3$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\varphi_1\equiv \varphi_2$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\varphi_0\equiv \varphi_3$}
		\AxiomC{$\varphi_3\equiv \varphi_4$}
		\AxiomC{}
		\LeftLabel{$\vee comp$}
		\UnaryInfC{$\varphi_4\equiv \varphi_5$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$\varphi_3\equiv \varphi_5$}
		\LeftLabel{$\_\sq\_$}
		\BinaryInfC{$a\vee\top\equiv\top$}
		\DisplayProof
	\end{center}
	For implementation see Code \ref{src:bool_andrdist}.
\end{proof}
 
Based on these theorems we can lay the foundation for the other descriptions of classical propositional logic. For further proofs see Code \ref{src:bool_orabs}.

\subsection{Conclusion about the Boolean system}

We can see that the Boolean system is different. We need to think in small steps, how to reach the other side of an equation. Sometimes it is needed to plan the proof multiple steps ahead. Compared to the natural deduction, it is harder to use, but compared to the Hamilton's or Nicod's system, it is much easier due to more logical connectives are available.