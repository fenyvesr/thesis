\section{Natural deduction}\label{sec:fs_nd}

In the natural deduction system we distinguish between hypotheses and consequences. The antecedents or hypotheses are separated from the succedents or consequences by the assertion-sign. In the natural deduction system, we are not only stating absolute assertions rather we are stating that consequences are asserted if the hypotheses are asserted. The collection of hypotheses are denoted with capital Greek letters (such as $\Gamma,\Delta$) when their exact composition is not relevant.

To describe the next system, six axioms and twelve inference rules are introduced. We call them natural deduction system.

\subsection{Informal description of the natural deduction system}

We can describe classical propositional logic as the following system, $\Omega=\Omega_0\cup\Omega_2=\{\cdot,\top,\bot\}\cup\{\_,\_\ ,\ \_\Rightarrow\_\ ,\ \_\wedge\_\ ,\ \_\vee\_\ \}$, $Z = \{\ \_\circ\_\ ,\ \_[\_]\ ,\ \_,\_\ , exfalso, lam, app, pair, fst, snd,  left, right, case \}$ and $I = \{id,\varepsilon,p,q, tt, lem\}$, where the terms are the followings.
\begin{center}
	\AxiomC{$\Theta\vdash\Delta$}
	\AxiomC{$\Gamma\vdash\Theta$}
	\LeftLabel{$\_\circ\_$}
	\BinaryInfC{$\Gamma\vdash\Delta$}
	\DisplayProof
	\quad
	\AxiomC{$\Delta\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\Delta$}
	\LeftLabel{$\_[\_]$}
	\BinaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
	\quad
	\AxiomC{$\Gamma\vdash\Delta$}
	\AxiomC{$\Gamma\vdash\varphi$}
	\LeftLabel{$\_,\_$}
	\BinaryInfC{$\Gamma\vdash(\Delta,\varphi)$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\Gamma\vdash\bot$}
	\LeftLabel{exfalso}
	\UnaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$(\Gamma,\varphi)\vdash\psi$}
	\LeftLabel{lam}
	\UnaryInfC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\DisplayProof
	\quad
	\AxiomC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\LeftLabel{app}
	\UnaryInfC{$(\Gamma,\varphi)\vdash\psi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\Gamma\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{pair}
	\BinaryInfC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\DisplayProof
	\quad
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{fst}
	\UnaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
	\quad
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{snd}
	\UnaryInfC{$\Gamma\vdash\psi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\Gamma\vdash\varphi$}
	\LeftLabel{left}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
	\quad
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{right}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
\end{center}
\begin{prooftree}
	\def\defaultHypSeparation{\hskip .125cm}
	\AxiomC{$(\Gamma,\varphi)\vdash\chi$}
	\AxiomC{$(\Gamma,\psi)\vdash\chi$}
	\AxiomC{$\Gamma\vdash(\varphi\vee\psi)$}
	\LeftLabel{case}
	\TrinaryInfC{$\Gamma\vdash\chi$}
\end{prooftree}
\begin{center}
	\AxiomC{}
	\LeftLabel{id}
	\UnaryInfC{$\Gamma\vdash\Gamma$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{$\varepsilon$}
	\UnaryInfC{$\Gamma\vdash\cdot$}
	\DisplayProof
	\
	\AxiomC{}
	\LeftLabel{p}
	\UnaryInfC{$(\Gamma,\varphi)\vdash\Gamma$}
	\DisplayProof
	\
	\AxiomC{}
	\LeftLabel{q}
	\UnaryInfC{$(\Gamma,\varphi)\vdash\varphi$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{tt}
	\UnaryInfC{$\Gamma\vdash\top$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{lem}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee(\varphi\Rightarrow\bot))$}
	\DisplayProof
\end{center}

The $\_[\_]$ rule gives the transitive property of natural deduction. If $\varphi$ can be deduced from $\Delta$ hypotheses and $\Delta$ hypotheses can be deduced from $\Gamma$ hypotheses, then $\varphi$ can be deduced from $\Gamma$ hypotheses. This rule makes it possible to create sequences from proofs. $\_\circ\_$ is the general version of $\_[\_]$, instead of stating one consequence, $\_\circ\_$ is true for a set of consequences.

$exfalso$ states that, if the logical constant $\bot$ can be asserted by a set of hypotheses, then anything can be asserted. Meaning if the hypotheses themselves are contradictory, then anything can be concluded from them.

The $lam$ and $app$ rules are the introduction and elimination rules for $\_\Rightarrow\_$. They also create a connection between the hypotheses and consequences. These rules are creating a path for the formulas to move between the left and right side of the assertion-sign.

The $pair$, $fst$ and $snd$ rules are the introduction and elimination rules for $\_\wedge\_$. Instead of proving $\varphi\wedge\psi$ to be true by the given $\Gamma$ hypotheses, it is enough to prove $\varphi$ and $\psi$ to be true by the same hypotheses. On the other hand if $\varphi\wedge\psi$ is proven to be true, then $\varphi$ and $\psi$ can be proven to be true as well.

The $right$, $left$ and $case$ rules are the introduction and elimination rules for $\_\vee\_$.

$\cdot$ represents an empty list of hypotheses, the $\_,\_$ connective is used to add another formula to the already existing list of hypotheses. $\varepsilon$ states that the empty list can always be proven independently from the given hypotheses. $p$ eliminates $\_,\_$ by stating that the first part of the list can be proven by the list itself. $q$ is the opposite by stating that the last element of a list can be proven by the list itself.

$id$ states that everything can be proven by itself. $tt$ states that the logical constant $\top$ can be asserted by any hypotheses. $lem$ is an acronym which stands for "Law of excluded middle", also known as tertium non datur. $lem$ states that for any $\varphi$ formula either $\varphi$ is true or $\neg\varphi$ is true. This makes this system classical logic. Without $lem$ the system describes intuitionistic logic or sometimes called constructive logic. Constructive logical formulas can have more than two values.

In classical logic, propositional formulas are either true or false. Although we know that every formula is either true or false proving it can be really hard. In contrast, propositional formulas in intuitionistic logic are not assigned a definite truth value and are only considered true when we have direct evidence, a proof.
 
In the natural deduction system, $\neg$ connective can be expressed by $\Rightarrow$ and $\bot$ (see Code \ref{src:nd_neg}).
\begin{align*}
	\neg\varphi&:=\varphi\Rightarrow\bot
\end{align*}

Let us dive into the system described above. We prove some basic theorems by hand.

\subsection{Proofs in natural deduction system}

Let us prove first that the last two elements of a list can be swapped.

\begin{theorem}
	\label{th:nd_comm12}
	For any $\Gamma,\varphi,\psi\in \mathcal{L}$, we can prove that $(\Gamma,\varphi,\psi)\vdash(\Gamma,\psi,\varphi)$.
\end{theorem}
\begin{proof}
	Let $\Gamma,\varphi,\psi\in \mathcal{L}$. To keep the size of the proof under control, let us introduce $\Gamma_{\varphi,\psi}:=(\Gamma,\varphi,\psi)$, $\Gamma_{\psi,\varphi}:=(\Gamma,\psi,\varphi)$, $\Gamma_\varphi:=(\Gamma,\varphi)$ and $\Gamma_\psi:=(\Gamma,\psi)$.
	\begin{prooftree}
		\def\defaultHypSeparation{\hskip .5cm}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_{\varphi}\vdash\Gamma$}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_{\varphi}$}
		\LeftLabel{$\_\circ\_$}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma$}
		\AxiomC{}
		\LeftLabel{q}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\psi$}
		\LeftLabel{$\_,\_$}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_{\psi}$}
		\AxiomC{}
		\LeftLabel{q}
		\UnaryInfC{$\Gamma_{\varphi}\vdash\varphi$}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_{\varphi}$}
		\LeftLabel{$\_[\_]$}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\varphi$}
		\LeftLabel{$\_,\_$}
		\BinaryInfC{$(\Gamma,\varphi,\psi)\vdash(\Gamma,\psi,\varphi)$}
	\end{prooftree}
\end{proof}

\subsection{Conclusion about the natural deduction system} 
The natural deduction system is more suitable for building proof trees, which representation is close to how humans deduce a proof by connecting already existing proofs together. In the Hamilton's and Nicod's system where node has exactly two antecedents and one consequence making the proof trees binary, while natural deduction proof trees are more diverse.