\section{Formal comparison of natural deduction and Boolean categories}

The last equivalence to prove in this thesis is between the natural deduction and Boolean category. If we prove this equivalence, then we proved the equivalence of all the previously discussed categories since equivalence is transitive. To finish this, we need to define first what a Boolean category is.

\subsection{Boolean category}

The objects in Boolean category are Boolean algebras. A Boolean algebra can be formally defined the following way (see Code \ref{src:bool_alg}).
\begin{align*}
	Form &: Set\\
	\neg&:Form\rightarrow Form
\end{align*}
\begin{align*}
	pv &: PV \rightarrow Form&
	\iota &: PV\\
	\top&:Form&
	\bot&:Form\\
	\_\wedge\_&:Form\rightarrow Form\rightarrow Form&
	\_\vee\_&:Form\rightarrow Form\rightarrow Form\\
	\wedge ass&:a\wedge(b\wedge c)\equiv(a\wedge b)\wedge c&
	\vee ass&:a\vee(b\vee c)\equiv(a\vee b)\vee c\\
	\wedge com&:a\wedge b\equiv b\wedge a&
	\vee com&:a\vee b\equiv b\vee a\\
	\wedge id&:a\wedge\top\equiv a&
	\vee id&:a\vee\bot\equiv a\\
	\wedge dist&:a\wedge(b\vee c)\equiv(a\wedge b)\vee(a\wedge c)&
	\vee dist&:a\vee(b\wedge c)\equiv(a\vee b)\wedge(a\vee c)\\
	\wedge comp&:a\wedge\neg a\equiv\bot&
	\vee comp&:a\vee\neg a\equiv\top\\
\end{align*}

We can notice the difference from the previous algebraic descriptions. The Boolean algebra has only axioms, no explicit inference rules. The inference rules are inherited from the $\_\equiv\_$ equality, meaning that it is reflexive, symmetric and transitive. We can use these properties as implicit inference rules. 

Similarly to the previous algebras, we can define homomorphism between Boolean algebras. We only have to define how does a homomorphism from Boolean algebra $X$ to Boolean algebra $Y$ transform the fields of the algebra. A homomorphism between Boolean algebras is formally defined the following way (see Code \ref{src:bool_hom}).
\begin{align*}
	\llbracket\_\rrbracket &: Form^X \rightarrow Form^Y\\
	\neg&:\llbracket\neg^X a\rrbracket\equiv\neg^Y\llbracket a\rrbracket
\end{align*}
\begin{align*}
	pv &: \llbracket pv^X\ x\rrbracket\equiv pv^Y\llbracket x\rrbracket&
	\iota &: \iota^X\equiv\iota^Y\\
	\top &: \llbracket \top^X\rrbracket\equiv\top^Y&
	\bot &: \llbracket \top^X\rrbracket\equiv\bot^Y\\
	\_\wedge\_&:\llbracket a\wedge^X b\rrbracket\equiv\llbracket a\rrbracket\wedge^Y\llbracket b\rrbracket&
	\_\vee\_&:\llbracket a\vee^X b\rrbracket\equiv\llbracket a\rrbracket\vee^Y\llbracket b\rrbracket
\end{align*}

Now we can define the Boolean category using the algebras as objects and the homomorphisms as morphisms. It is fairly easy to define the identity morphism and the composition of morphisms. The formal definition of Boolean category can be seen at Code \ref{src:bool_cat}.

\subsection{The equivalence of natural deduction category and Boolean category} 

How can we specify a functor from natural deduction category to the Boolean category and in the opposite direction? The formulas are almost the same with using the same logical connectives, but the assertion of formulas in the previous algebras are different from the notion of formula equalities. We explain $\vdash\varphi$ as $\top\equiv\varphi$. So a formula can be asserted only if it is equal to the logical constant truth.

Let us specify a Boolean algebra with a categorical natural deduction algebra. As mentioned earlier the $Form$, $pv$, $\iota$, $\top$, $\bot$, $\_\wedge\_$ and $\_\vee\_$ fields of the Boolean algebra can be identical to the corresponding fields in the categorical natural deduction.
$\neg$ can be specified by using $\_\Rightarrow\_$ and $\bot$ as earlier in the Hamilon-natural deduction case.

Based on these constraints the axioms of the Boolean algebra can be defined by the axioms and inference rules of the categorical natural deduction algebra. To prove the equalities in the Boolean algebra, the Lindenbaum-Tarski congruence of the categorical natural deduction algebra has to be used. This helps proving the equalities by providing two assertions instead, which then have to be proven (see Code \ref{src:nd_bool}).

For example instead of proving $a\vee\neg a\equiv\top$, by using $cong$ we have to prove $a\vee(a\Rightarrow\bot)\vdash\top$ and $\top\vdash(a\Rightarrow(a\Rightarrow\bot))$. Which is pretty easy, since the first one is the $tt$ axiom, while the second one is the $lem$ axiom of the categorical natural deduction algebra.

So we were able to provide a transformation from categorical natural deduction algebras to Boolean algebras. Now let us investigate the transformation of homomorphism based on this specification.

We only have to prove that the $\neg$ field of a homomorphism is satisfied, due to almost every logical connective is identical in the two algebras.
$\llbracket a\Rightarrow^X\bot^X\rrbracket \equiv \llbracket a\rrbracket\Rightarrow^Y\bot^Y$ has to be proven.

\begin{center}
	\small
	\AxiomC{}
	\LeftLabel{$Hom.\_\Rightarrow\_$}
	\UnaryInfC{$\llbracket a\Rightarrow^X\bot^X\rrbracket\equiv \llbracket a\rrbracket\Rightarrow^Y\llbracket\bot^X\rrbracket$}
	\AxiomC{$\_\Rightarrow^Y\_$}
	\AxiomC{}
	\LeftLabel{$Hom.\bot$}
	\UnaryInfC{$\llbracket\bot^X\rrbracket\equiv\bot^Y$}
	\LeftLabel{ap2snd}
	\BinaryInfC{$\llbracket a\rrbracket\Rightarrow^Y\llbracket\bot^X\rrbracket \equiv \llbracket a\rrbracket\Rightarrow^Y\bot^Y$}
	\LeftLabel{id}
	\BinaryInfC{$\llbracket a\Rightarrow^X\bot^X\rrbracket \equiv \llbracket a\rrbracket\Rightarrow^Y\bot^Y$}
	\DisplayProof
\end{center}

So we specified a functor from the natural deduction category to the Boolean category. Let us do the same for the opposite direction.

First let us specify how the functor transforms the objects. We will specify a categorical natural deduction algebra starting from a Boolean algebra. Almost every logical connective can be identical to the corresponding Boolean ones. We only have to specify the $\_\Rightarrow\_$ and $\_\vdash\_$ fields of the categorical natural deduction algebra.
\begin{align*}
	\varphi\Rightarrow\psi&=
	(\neg\varphi)\vee\psi&
	\Gamma\vdash\varphi&=\Gamma\equiv\Gamma\wedge\varphi
\end{align*}

This effects the algebra the following way. We have to provide proofs for the following terms.
\begin{align*}
	id&: \Gamma\equiv\Gamma\wedge\Gamma\\
	\_[\_]&: \Delta\equiv\Delta\wedge\varphi
	\rightarrow
	\Gamma\equiv\Gamma\wedge\Delta
	\rightarrow
	\Gamma\equiv\Gamma\wedge\varphi\\
	tt&:\Gamma\equiv\Gamma\wedge\top\\
	pair&:\Gamma\equiv\Gamma\wedge\varphi
	\rightarrow
	\Gamma\equiv\Gamma\wedge\psi
	\rightarrow
	\Gamma\equiv\Gamma\wedge(\varphi\wedge\psi)\\
	fst&:\Gamma\equiv\Gamma\wedge(\varphi\wedge\psi)
	\rightarrow
	\Gamma\equiv\Gamma\wedge\varphi\\
	snd&:\Gamma\equiv\Gamma\wedge(\varphi\wedge\psi)
	\rightarrow
	\Gamma\equiv\Gamma\wedge\psi\\
	lam&:(\Gamma\wedge\varphi)\equiv(\Gamma\wedge\varphi)\wedge\psi
	\rightarrow
	\Gamma\equiv\Gamma\wedge((\neg\varphi)\vee\psi)\\
	app&:\Gamma\equiv\Gamma\wedge((\neg\varphi)\vee\psi)
	\rightarrow
	(\Gamma\wedge\varphi)\equiv(\Gamma\wedge\varphi)\wedge\psi\\
	left&:\Gamma\equiv\Gamma\wedge\varphi\rightarrow\Gamma\equiv\Gamma\wedge(\varphi\vee\psi)\\
	right&:\Gamma\equiv\Gamma\wedge\psi\rightarrow\Gamma\equiv\Gamma\wedge(\varphi\vee\psi)\\
	case&:(\Gamma\wedge\varphi)\equiv(\Gamma\wedge\varphi)\wedge\chi
	\rightarrow
	(\Gamma\wedge\psi)\equiv(\Gamma\wedge\psi)\wedge\chi
	\rightarrow
	\Gamma\equiv\Gamma\wedge(\varphi\vee\psi)
	\rightarrow\\&\quad\rightarrow
	\Gamma\equiv\Gamma\wedge\vdash\chi\\
	exfalso&:\Gamma\equiv\Gamma\wedge((\neg\bot)\vee\varphi)\\
	lem&:\Gamma\equiv\Gamma\wedge(\varphi\vee((\neg\varphi)\vee\bot))\\
	cong &: \varphi\equiv\varphi\wedge\psi
	\rightarrow
	\psi\equiv\psi\wedge\varphi
	\rightarrow
	\varphi\equiv \psi
\end{align*} 

There are some easier proofs, such as $tt$ which is the symmetric version of $\wedge id$. For the proofs see Code \ref{src:bool_nd}. So based on the specification of the logical connectives, we were able to prove the needed inference rules and axioms.

What effect does it have on the homomorphisms? We can specify a categorical natural deduction homomorphism based on a Boolean homomorphism.

Again, almost all fields of the categorical natural deduction homomorphism is identical to the corresponding fields in the Boolean homomorphism. Only the transformation of proofs $\llbracket\_\rrbracket_p$ and the $\_\Rightarrow\_$ fields have to be discussed.

So first we have to prove that $\llbracket\Gamma\rrbracket\equiv\llbracket\Gamma\rrbracket\wedge^Y\llbracket\varphi\rrbracket$ if $\Gamma\equiv\Gamma\wedge^X\varphi$.
\begin{center}
	\small
	\AxiomC{$\llbracket\_\rrbracket$}
	\AxiomC{}
	\LeftLabel{hyp}
	\UnaryInfC{$\Gamma\equiv\Gamma\wedge^X\varphi$}
	\LeftLabel{ap}
	\BinaryInfC{$\llbracket\Gamma\rrbracket\equiv\llbracket\Gamma\wedge^X\varphi\rrbracket$}
	\AxiomC{}
	\LeftLabel{$Hom.\_\wedge\_$}
	\UnaryInfC{$\llbracket\Gamma\wedge^X\varphi\rrbracket\equiv\llbracket\Gamma\rrbracket\wedge^Y\llbracket\varphi\rrbracket$}
	\LeftLabel{$\_\sq\_$}
	\BinaryInfC{$\llbracket\Gamma\rrbracket\equiv\llbracket\Gamma\rrbracket\wedge^Y\llbracket\varphi\rrbracket$}
	\DisplayProof
\end{center}
Proving $\_\Rightarrow\_$ happens the same way as earlier so many times. We have to break down the equation along the logical connectives.
\begin{center}
	\small
	\AxiomC{}
	\LeftLabel{$Hom.\_\vee\_$}
	\UnaryInfC{$\llbracket(\neg\varphi)\vee^X\psi\rrbracket\equiv\llbracket\neg\varphi\rrbracket\vee^Y\llbracket\psi\rrbracket$}
	\AxiomC{$\_\vee^Y\_$}
	\AxiomC{}
	\LeftLabel{$Hom.\neg$}
	\UnaryInfC{$\llbracket\neg\varphi\rrbracket\equiv\neg^Y\llbracket\varphi\rrbracket$}
	\LeftLabel{ap2fst}
	\BinaryInfC{$\llbracket\neg\varphi\rrbracket\vee^Y\llbracket\psi\rrbracket\equiv(\neg^Y\llbracket\varphi\rrbracket)\vee^Y\llbracket\psi\rrbracket$}
	\LeftLabel{$\_\sq\_$}
	\BinaryInfC{$\llbracket(\neg\varphi)\vee^X\psi\rrbracket\equiv(\neg^Y\llbracket\varphi\rrbracket)\vee^Y\llbracket\psi\rrbracket$}
	\DisplayProof
\end{center}

We arrived at the point where the two functors are specified. Now we have to prove that each object is naturally isomorphic to its image under the composition of these two functors.

The $f^\beta$ field of the equivalence requires an isomorphism between a Boolean algebra and its image. $\llbracket\_\rrbracket$, $pv$, $\iota$, $\top$, $\bot$, $\_\wedge\_$ and $\_\vee\_$ are trivial since none of the functors modify them. We only have to prove that the image of a negation is equal to the original formula.
\begin{center}
	\small
	\AxiomC{}
	\LeftLabel{$\vee id$}
	\UnaryInfC{$(\neg\varphi)\vee\bot\equiv\neg\varphi$}
	\DisplayProof
\end{center}

The $f^\eta$ field of the equivalence requires an isomorphism between a categorical natural deduction algebra and its image. $\llbracket\_\rrbracket$, $pv$, $\iota$, $\top$, $\_\wedge\_$, $\_\vee\_$ and $\bot$ are trivial since none of the functors modify them. We only have to prove $\llbracket\_\rrbracket_p$ and $\_\Rightarrow\_$ (see Code \ref{src:nd_bool}).

With providing all the specification and the proofs required by them, we can declare that the natural deduction category and the Boolean category are equivalent. This means we can state that all the four different categories are equivalent.

We could not only prove that the syntax of the different categories are equivalent, by providing bidirectional transformations from the syntaxes, but we were able to prove that any semantics of the different categories can be mapped into another category and providing a semantics there. The first approach only states that one particular algebra can be mapped onto one particular algebra in the other category and vice versa, while the second approach targets every algebra and maps them into the other category. This way we could prove the equivalence of the categories, not just the equivalence of one particular algebra inside the categories.