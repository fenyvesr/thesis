\section{Natural Deduction}

As seen before with Nicod's and Hamilton's systems, we needed some restrictions in the algebra. The same applies for the natural deduction system. The natural deduction system defined at Section \ref{sec:fs_nd} defines a more numerous language than the Lindenbaum-Tarski quotient of the Nicod's and Hamilton's system. We will restrain the natural deduction system by eliminating the difference between the left and right side of the assertions-sign. We can do so by defining $\cdot$ to be $\top$ and $\_,\_$ to be the $\_\wedge\_$ logical connective. Applying these changes we can see, that the following inference rules become obsolete. $\_,\_$ rule transforms into $\_[\_]$, $\varepsilon$ rule becomes $tt$, $\_\circ\_$ rule will be identical to $pair$. Also $p$ and $q$ disappears due to the eradication of $\_,\_$ connective, so they shall be proved later.

The formal system which we are left with contains four axioms and nine inference rules. We will call them categorical natural deduction system.

\subsection{Formal definition of categorical natural deduction system}

We can define propositional logic as the following formal system, $\Omega=\Omega_0\cup\Omega_2=\{\top,\bot\}\cup\{\Rightarrow,\wedge,\vee\}$, $Z = \{ [\;] , pair, fst, snd, lam, app, right, left, case \}$ and $I = \{id, tt, exfalso, lem\}$, where the terms are defined as
\begin{prooftree}
	\AxiomC{$\Delta\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\Delta$}
	\LeftLabel{\_[\_]}
	\BinaryInfC{$\Gamma\vdash\varphi$}
\end{prooftree}
\begin{center}
	\AxiomC{$\Gamma\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{pair}
	\BinaryInfC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{fst}
	\UnaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{snd}
	\UnaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$(\Gamma\wedge\varphi)\vdash\psi$}
	\LeftLabel{lam}
	\UnaryInfC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\LeftLabel{app}
	\UnaryInfC{$(\Gamma\wedge\varphi)\vdash\psi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{right}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash\varphi$}
	\LeftLabel{left}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
\begin{prooftree}
	\def\defaultHypSeparation{\hskip .125cm}
	\AxiomC{$(\Gamma\wedge\varphi)\vdash\chi$}
	\AxiomC{$(\Gamma\wedge\psi)\vdash\chi$}
	\AxiomC{$\Gamma\vdash(\varphi\vee\psi)$}
	\LeftLabel{case}
	\TrinaryInfC{$\Gamma\vdash\chi$}
\end{prooftree}
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{id}
	\UnaryInfC{$\Gamma\vdash\Gamma$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{tt}
	\UnaryInfC{$\Gamma\vdash\top$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{exfalso}
	\UnaryInfC{$\Gamma\vdash(\bot\Rightarrow\varphi)$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{lem}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee(\varphi\Rightarrow\bot))$}
	\DisplayProof
\end{center}

The $\_[\_]$ rule gives the transitive property of natural deduction. If $\varphi$ can be deduced from $\Delta$ and $\Delta$ can be deduced from $\Gamma$, then $\varphi$ can be deduced from $\Gamma$. This rule makes it possible to create a sequence from the proofs.

Let us prove $p$ and $q$ rules, to make our life easier in the future.

\begin{theorem}
	\label{th:nd_p}
	For any $\Gamma,\varphi\in \mathcal{L}$, we can prove
	\AxiomC{}
	\LeftLabel{p}
	\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma$}
	\DisplayProof
	and
	\AxiomC{}
	\LeftLabel{q}
	\UnaryInfC{$\Gamma\wedge\varphi\vdash\varphi$}
	\DisplayProof.
\end{theorem}
\begin{proof}
	Let $\Gamma,\varphi\in \mathcal{L}$.
	\begin{center}
		\AxiomC{}
		\LeftLabel{id}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma\wedge\varphi$}
		\LeftLabel{fst}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma$}
		\DisplayProof
		\quad
		\AxiomC{}
		\LeftLabel{id}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma\wedge\varphi$}
		\LeftLabel{snd}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\varphi$}
		\DisplayProof
	\end{center}
	
	For implementation see Code \ref{src:nd_p} and Code \ref{src:nd_q}.
\end{proof}

The second proof will be also about the list of hypotheses. We will prove that the last two elements of a list can be swapped.

\begin{theorem}
	\label{th:nd_comm12}
	For any $\Gamma,\varphi,\psi\in \mathcal{L}$, we can prove that $(\Gamma,\varphi,\psi)\vdash(\Gamma,\psi,\varphi)$.
\end{theorem}
\begin{proof}
	Let $\Gamma,\varphi,\psi\in \mathcal{L}$. To make the proof smaller, let us have shorter notations. We denote $\Gamma_{\varphi,\psi}=(\Gamma,\varphi,\psi)$, $\Gamma_{\psi,\varphi}=(\Gamma,\psi,\varphi)$, $\Gamma_\varphi=(\Gamma,\varphi)$ and $\Gamma_\psi=(\Gamma,\psi)$.
	\begin{prooftree}
		\def\defaultHypSeparation{\hskip .5cm}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_\varphi\vdash\Gamma$}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_\varphi$}
		\LeftLabel{\_[\_]}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma$}
		\AxiomC{}
		\LeftLabel{q}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\psi$}
		\LeftLabel{pair}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_\psi$}
		\AxiomC{}
		\LeftLabel{q}
		\UnaryInfC{$\Gamma_\varphi\vdash\varphi$}
		\AxiomC{}
		\LeftLabel{p}
		\UnaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_\varphi$}
		\LeftLabel{\_[\_]}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\varphi$}
		\LeftLabel{pair}
		\BinaryInfC{$\Gamma_{\varphi,\psi}\vdash\Gamma_{\psi,\varphi}$}
	\end{prooftree}
	
	For implementation see Code \ref{src:nd_comm12}.
\end{proof}
