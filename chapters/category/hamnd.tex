\section{Formal comparison of Hamilton and natural deduction categories}

After investigating the equivalence of the Nicod and Hamilton categories let us move forward and prove the equivalence of the Hamilton and natural deduction categories. If it can be proved, then it also means that the Nicod and the natural deduction categories are equivalent since equivalence is transitive.

First let us formally define the natural deduction category, then prove its equivalence to the Hamilton category. We have to notice that some extra restrictions are needed. How can we transform a Hamilton formula into a natural deduction formula? We saw how the logical connectives can be expressed by each other. Let us examine a small transformation from a Hamilton formula to a natural deduction formula, this natural deduction formula to its Hamilton version and so on.
$$
\overbrace{\varphi\Rightarrow\psi}^{Hamilton} \mapsto
\overbrace{\varphi\Rightarrow\psi}^{natural deduction} \mapsto
\overbrace{\varphi\Rightarrow\psi}^{Hamilton} \mapsto
\overbrace{\varphi\Rightarrow\psi}^{natural deduction}\mapsto
\cdots
$$
Comparing with the Nicod case, it seems promising. This formula did not "explode". What about the negation?
$$
\overbrace{\neg\varphi}^{Hamilton} \mapsto
\overbrace{\varphi\Rightarrow\bot}^{natural deduction} \mapsto
\overbrace{\varphi\Rightarrow\neg((pv\ \iota)\Rightarrow(pv\ \iota))}^{Hamilton} \mapsto
\overbrace{\varphi\Rightarrow(((pv\ \iota)\Rightarrow(pv\ \iota))\Rightarrow\bot)}^{natural deduction}\mapsto
\cdots
$$
Here we can see, that this formula "explodes", so the Lindenbaum-Tarski congruence is needed again to prove the equivalence.

What is the case if we start with a natural deduction formula. Let us examine the opposite direction.
$$
\overbrace{\varphi\Rightarrow\psi}^{natural deduction} \mapsto
\overbrace{\varphi\Rightarrow\psi}^{Hamilton} \mapsto
\overbrace{\varphi\Rightarrow\psi}^{natural deduction}\mapsto
\overbrace{\varphi\Rightarrow\psi}^{Hamilton} \mapsto
\cdots
$$
Again, a formula containing implication is unaffected by the transformations.
$$
\overbrace{\varphi\vee\psi}^{natural deduction} \mapsto
\overbrace{\neg\varphi\Rightarrow\psi}^{Hamilton} \mapsto
\overbrace{(\varphi\Rightarrow\bot)\Rightarrow\psi}^{natural deduction}\mapsto
\overbrace{(\varphi\Rightarrow\neg((pv\ \iota)\Rightarrow(pv\ \iota)))\Rightarrow\psi}^{Hamilton} \mapsto
\cdots
$$
We can see that transforming a disjunction also "explodes". The same is true for a conjunction.

So based on these short transformations we can conclude that the Lindenbaum-Tarski quotient is needed in the natural deduction algebra as well. It results in an extra $cong$ rule in the algebra.

\subsection{Natural deduction category}
In categorical natural deduction algebra, we need extra restrictions, not just $cong$, since the natural deduction system defined at Section \ref{sec:fs_nd} is a more detailed language than the Lindenbaum-Tarski quotient of the Nicod's and Hamilton's system. We restrain the natural deduction system by eliminating the difference between the left and right side of the assertion-sign. We can do so by defining $\cdot$ to be $\top$ and $\_,\_$ to be the $\_\wedge\_$ logical connective. Applying these changes we can see, that the following inference rules become obsolete. $\_,\_$ rule transforms into $\_[\_]$, $\varepsilon$ rule becomes $tt$ and $\_\circ\_$ rule is now identical to $pair$. Also $p$ and $q$ disappear due to the eradication of $\_,\_$ connective, so they shall be proved later. We call this restrained version of natural deduction system as categorical natural deduction system.

The system, which we are left with, contains four axioms and nine inference rules. We can describe classical propositional logic as the following system, $\Omega=\Omega_0\cup\Omega_2=\{\top,\bot\}\cup\{\Rightarrow,\wedge,\vee\}$, $Z = \{ [\;] , pair, fst, snd, lam, app, right, left, case \}$ and $I = \{id, tt, exfalso, lem\}$, where the terms are the followings.
\begin{prooftree}
	\AxiomC{$\Delta\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\Delta$}
	\LeftLabel{\_[\_]}
	\BinaryInfC{$\Gamma\vdash\varphi$}
\end{prooftree}
\begin{center}
	\AxiomC{$\Gamma\vdash\varphi$}
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{pair}
	\BinaryInfC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{fst}
	\UnaryInfC{$\Gamma\vdash\varphi$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\wedge\psi)$}
	\LeftLabel{snd}
	\UnaryInfC{$\Gamma\vdash\psi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$(\Gamma\wedge\varphi)\vdash\psi$}
	\LeftLabel{lam}
	\UnaryInfC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash(\varphi\Rightarrow\psi)$}
	\LeftLabel{app}
	\UnaryInfC{$(\Gamma\wedge\varphi)\vdash\psi$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\Gamma\vdash\psi$}
	\LeftLabel{right}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
	\ 
	\AxiomC{$\Gamma\vdash\varphi$}
	\LeftLabel{left}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee\psi)$}
	\DisplayProof
\end{center}
\begin{prooftree}
	\def\defaultHypSeparation{\hskip .125cm}
	\AxiomC{$(\Gamma\wedge\varphi)\vdash\chi$}
	\AxiomC{$(\Gamma\wedge\psi)\vdash\chi$}
	\AxiomC{$\Gamma\vdash(\varphi\vee\psi)$}
	\LeftLabel{case}
	\TrinaryInfC{$\Gamma\vdash\chi$}
\end{prooftree}
\begin{center}
	\AxiomC{}
	\LeftLabel{id}
	\UnaryInfC{$\Gamma\vdash\Gamma$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{tt}
	\UnaryInfC{$\Gamma\vdash\top$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{exfalso}
	\UnaryInfC{$\Gamma\vdash(\bot\Rightarrow\varphi)$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{lem}
	\UnaryInfC{$\Gamma\vdash(\varphi\vee(\varphi\Rightarrow\bot))$}
	\DisplayProof
\end{center}

Let us prove $p$ and $q$ rules, to get familiar with this description of natural deduction.

\begin{theorem}
	\label{th:nd_p}
	For any $\Gamma,\varphi\in \mathcal{L}$, we can prove
	\AxiomC{}
	\LeftLabel{p}
	\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma$}
	\DisplayProof
	and
	\AxiomC{}
	\LeftLabel{q}
	\UnaryInfC{$\Gamma\wedge\varphi\vdash\varphi$}
	\DisplayProof.
\end{theorem}
\begin{proof}
	Let $\Gamma,\varphi\in \mathcal{L}$.
	\begin{center}
		\AxiomC{}
		\LeftLabel{id}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma\wedge\varphi$}
		\LeftLabel{fst}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma$}
		\DisplayProof
		\quad
		\AxiomC{}
		\LeftLabel{id}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\Gamma\wedge\varphi$}
		\LeftLabel{snd}
		\UnaryInfC{$\Gamma\wedge\varphi\vdash\varphi$}
		\DisplayProof
	\end{center}
	
	For implementation see Code \ref{src:nd_p} and Code \ref{src:nd_q}.
\end{proof}

Now we arrived at the point where we are able to formally define the categorical natural deduction algebra, its homomorphisms and based on these two the natural deduction category.

Categorical natural deduction algebra can be defined the following way (see Code \ref{src:nd_alg})
\begin{align*}
	Form &: Set\\
	\_\vdash\_&: Form\rightarrow Form\rightarrow Prop\\
	id&: \Gamma\vdash\Gamma\\
	\_[\_]&: \Delta\vdash\varphi\rightarrow\Gamma\vdash\Delta\rightarrow\Gamma\vdash\varphi\\
	pv &: PV \rightarrow Form\\
	\iota &: PV\\
	\top&:Form\\
	tt&:\Gamma\vdash\top\\
	\_\wedge\_&:Form\rightarrow Form\rightarrow Form\\
	pair&:\Gamma\vdash\varphi\rightarrow\Gamma\vdash\psi\rightarrow\Gamma\vdash(\varphi\wedge\psi)\\
	fst&:\Gamma\vdash(\varphi\wedge\psi)\rightarrow\Gamma\vdash\varphi\\
	snd&:\Gamma\vdash(\varphi\wedge\psi)\rightarrow\Gamma\vdash\psi\\
	\_\Rightarrow\_&:Form\rightarrow Form\rightarrow Form\\
	lam&:(\Gamma\wedge\varphi)\vdash\psi\rightarrow\Gamma\vdash(\varphi\Rightarrow\psi)\\
	app&:\Gamma\vdash(\varphi\Rightarrow\psi)\rightarrow(\Gamma\wedge\varphi)\vdash\psi\\
	\_\vee\_&:Form\rightarrow Form\rightarrow Form\\
	left&:\Gamma\vdash\varphi\rightarrow\Gamma\vdash(\varphi\vee\psi)\\
	right&:\Gamma\vdash\psi\rightarrow\Gamma\vdash(\varphi\vee\psi)\\
	case&:(\Gamma\wedge\varphi)\vdash\chi\rightarrow(\Gamma\wedge\psi)\vdash\chi\rightarrow\Gamma\vdash(\varphi\vee\psi)\rightarrow\Gamma\vdash\chi\\
	\bot&:Form\\
	exfalso&:\Gamma\vdash(\bot\Rightarrow\varphi)\\
	lem&:\Gamma\vdash(\varphi\Rightarrow(\varphi\Rightarrow\bot))\\
	cong &: \varphi\vdash\psi\rightarrow\psi\vdash\varphi\rightarrow \varphi\equiv \psi
\end{align*}

We can already see from the sheer size of the algebra, that this is more detailed and provides more tool to deduce proofs.

The definition of homomorphisms between categorical natural deduction algebras does not need to have so many rules as seen in the definition of the algebra itself. We only need to define how the homomorphism affects the formula set, the proofs and the logical connectives (see Code \ref{src:nd_hom}). A homomorphism from categorical natural deduction algebra $X$ to $Y$ can be formally defined as
\begin{align*}
	\llbracket\_\rrbracket &: Form^X\rightarrow Form^Y\\
	\llbracket\_\rrbracket_p&: \Gamma\vdash^X\varphi\rightarrow\llbracket\Gamma\rrbracket\vdash^Y \llbracket\varphi\rrbracket\\
	pv &: \llbracket pv^X\ x\rrbracket\equiv pv^Y\ x\\
	\iota &: \iota^X\equiv\iota^Y\\
	\top &: \llbracket\top^X\rrbracket\equiv\top^Y\\
	\_\Rightarrow\_&:\llbracket\varphi\Rightarrow^X\psi\rrbracket\equiv \llbracket\varphi\rrbracket\Rightarrow^Y\llbracket\psi\rrbracket\\
	\_\wedge\_&:\llbracket\varphi\wedge^X\psi\rrbracket\equiv\llbracket\varphi\rrbracket\wedge^Y\llbracket\psi\rrbracket\\
	\_\vee\_&:\llbracket\varphi\vee^X\psi\rrbracket\equiv\llbracket\varphi\rrbracket\vee^Y\llbracket\psi\rrbracket\\
	\bot &: \llbracket\bot^X\rrbracket\equiv\bot^Y
\end{align*}
With all these we can define the natural deduction category (see Code \ref{src:nd_cat}).

\subsection{Equivalence between Hamilton and natural deduction categories}

As previously seen at the proof of equivalence between Nicod and Hamilton categories, we only need to specify how the formulas are expressed in the different algebras. This determines what has to be proven later. Let us see how the different connectives in one category are expressed by the connectives used in the other category.

Let us specify a categorical natural deduction algebra from a Hamilton algebra. As it was discussed earlier, the $Form$, $pv$, $\iota$ and $cong$ fields of the categorical natural deduction algebra are identical to the corresponding fields in the Hamilton algebra.

The logical connectives of arity zero, so the logical constants can be specified the following way. In the Hamilton algebra, there are no logical constants, so we need to use the $\iota$ notation.
$\top$ can be specified as $(pv\ \iota)\Rightarrow(pv\ \iota)$. Informally we know, that this formula is always true, independently of $\iota$. This is a tautology. $\bot$ can be specified as the negation of $\top$. The logical connective of arity two can also be specified in accordance to the informal intuition about them.
As seen at the introduction of this Section, the implication will be the same in both algebras.

One major difference from the Nicod-Hamilton equivalence is that in the Hamilton-natural deduction equivalence, the assertion-sign is transformed as well. $\Gamma\vdash\varphi$ is specified as $\vdash\Gamma\Rightarrow\varphi$.

What type of inference rules have to be proven with this specification? The inference rules transform into the following system which has to be proven by the axioms given by the Hamilton algebra.
\begin{prooftree}
	\AxiomC{$\vdash(\Delta\Rightarrow\varphi)$}
	\AxiomC{$\vdash(\Gamma\Rightarrow\Delta)$}
	\LeftLabel{\_[\_]}
	\BinaryInfC{$\vdash(\Gamma\Rightarrow\varphi)$}
\end{prooftree}
\begin{center}
	\AxiomC{$\vdash(\Gamma\Rightarrow\varphi)$}
	\AxiomC{$\vdash(\Gamma\Rightarrow\psi)$}
	\LeftLabel{pair}
	\BinaryInfC{$\vdash(\Gamma\Rightarrow(\neg(\varphi\Rightarrow(\neg\psi)))$}
	\DisplayProof
	\ 
	\AxiomC{$\vdash(\Gamma\Rightarrow(\neg(\varphi\Rightarrow(\neg\psi))))$}
	\LeftLabel{fst}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow\varphi)$}
	\DisplayProof
	\ 
	\AxiomC{$\vdash(\Gamma\Rightarrow(\neg(\varphi\Rightarrow(\neg\psi))))$}
	\LeftLabel{snd}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow\psi)$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\vdash((\neg(\Gamma\Rightarrow(\neg\varphi)))\Rightarrow\psi)$}
	\LeftLabel{lam}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow(\varphi\Rightarrow\psi))$}
	\DisplayProof
	\ 
	\AxiomC{$\vdash(\Gamma\Rightarrow(\varphi\Rightarrow\psi))$}
	\LeftLabel{app}
	\UnaryInfC{$\vdash((\neg(\Gamma\Rightarrow(\neg\varphi)))\Rightarrow\psi)$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{$\vdash(\Gamma\Rightarrow\psi)$}
	\LeftLabel{right}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow((\neg\varphi)\Rightarrow\psi))$}
	\DisplayProof
	\ 
	\AxiomC{$\vdash(\Gamma\Rightarrow\varphi)$}
	\LeftLabel{left}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow((\neg\varphi)\Rightarrow\psi))$}
	\DisplayProof
\end{center}
\begin{prooftree}
	\def\defaultHypSeparation{\hskip .125cm}
	\AxiomC{$\vdash((\neg(\Gamma\Rightarrow(\neg\varphi)))\Rightarrow\chi)$}
	\AxiomC{$\vdash((\neg(\Gamma\Rightarrow(\neg\psi)))\Rightarrow\chi)$}
	\AxiomC{$\vdash(\Gamma\Rightarrow((\neg\varphi)\Rightarrow\psi))$}
	\LeftLabel{case}
	\TrinaryInfC{$\vdash(\Gamma\Rightarrow\chi)$}
\end{prooftree}
\begin{center}
	\AxiomC{}
	\LeftLabel{id}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow\Gamma)$}
	\DisplayProof
	\ 
	\AxiomC{}
	\LeftLabel{tt}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow((pv\ \iota)\Rightarrow(pv\ \iota)))$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{exfalso}
	\UnaryInfC{$\vdash(\Gamma\Rightarrow((\neg(((pv\ \iota)\Rightarrow(pv\ \iota))))\Rightarrow\varphi))$}
	\DisplayProof
\end{center}
\begin{center}
	\AxiomC{}
	\LeftLabel{lem}
	\UnaryInfC{$\vdash\Gamma\Rightarrow((\neg\varphi)\Rightarrow(\varphi\Rightarrow(\neg((pv\ \iota)\Rightarrow(pv\ \iota)))))$}
	\DisplayProof
\end{center}

We can see that $\_[\_]$, $id$ and $tt$ are pretty easy to prove. The first one is identical to the principle of syllogism, the second one is Theorem \ref{th:ham_id} and the third one is the $id$ applied on $L_1$. For the other proofs some additional inference rules are needed (see Code \ref{src:ham_nd}).

Let us continue with specifying a categorical natural deduction homomorphism. $\llbracket\_\rrbracket$, $pv$, $\iota$ and $\_\Rightarrow\_$ entities of a categorical natural deduction homomorphism can be specified the same as in the Hamilton homomorphism. Proving the other entities in the natural deduction homomorphism happens to be similar to the way we proved them in the Nicod-Hamilton case. The equations have to be disassembled along the logical connectives (see Code \ref{src:eqnicham}). Let us see the $\_\vee\_$ field for example.
\begin{center}
 	\AxiomC{$\llbracket(\neg^X\varphi)\Rightarrow^X\psi\rrbracket\equiv\llbracket\neg^X\varphi\rrbracket\Rightarrow^Y\llbracket\psi\rrbracket$}
 	\AxiomC{$\llbracket\neg^X\varphi\rrbracket\Rightarrow^Y\llbracket\psi\rrbracket\equiv(\neg^Y\llbracket\varphi\rrbracket)\Rightarrow^Y\llbracket\psi\rrbracket$}
 	\LeftLabel{$\_\sq\_$}
 	\BinaryInfC{$\llbracket(\neg^X\varphi)\Rightarrow^X\psi\rrbracket\equiv(\neg^Y\llbracket\varphi\rrbracket)\Rightarrow^Y\llbracket\psi\rrbracket$}
 	\DisplayProof
\end{center}

First, by using $\_\Rightarrow\_$ rule of the Hamilton homomorphism we can prove that $\llbracket(\neg^X\varphi)\Rightarrow^X\psi\rrbracket\equiv\llbracket\neg^X\varphi\rrbracket\Rightarrow^Y\llbracket\psi\rrbracket$. For the other proof we will need to prove that the matching sides of $\_\Rightarrow^Y\_$ are equal. 

\begin{center}
	\AxiomC{$\_\Rightarrow^Y\_$}
	\AxiomC{}
	\LeftLabel{$Hom.\neg$}
	\UnaryInfC{$\llbracket\neg^X\varphi\rrbracket\equiv\neg^Y\llbracket\varphi\rrbracket$}
	\LeftLabel{ap2fst}
	\BinaryInfC{$\llbracket\neg^X\varphi\rrbracket\Rightarrow^Y\llbracket\psi\rrbracket\equiv(\neg^Y\llbracket\varphi\rrbracket)\Rightarrow^Y\llbracket\psi\rrbracket$}
	\DisplayProof
\end{center}
We can analogously prove the other fields of the categorical natural deduction homomorphism. 

We specified a functor, which is capable of mapping the Hamilton category onto the natural deduction category (see Code \ref{src:eqhamnd}). Let us investigate the other direction. 

How can a Hamilton algebra be specified from a categorical natural deduction algebra? We have to first specify the logical connectives and the assertion-sign. Again, the implication can be untouched, the negation of a formula can be specified as the formula implies falsity, and the assertion of a formula can be specified as the formula can be deduced from the truth.
$$
\varphi\Rightarrow\psi:= \varphi\Rightarrow\psi
\quad
\neg\varphi:=\varphi\Rightarrow\bot
\quad
\vdash\varphi:=\top\vdash\varphi
$$
The $Form$, $pv$, $\iota$ and $\_\Rightarrow\_$ fields of the Hamilton algebra are identical to the corresponding fields in the categorical natural deduction algebra. The specification requires the following statements to be proven.
\begin{align*}
	\_\$\_ &: \top\vdash(p\Rightarrow q)\rightarrow \top\vdash p\rightarrow\top\vdash q\\
	L_1 &: \top\vdash(p\Rightarrow (q\Rightarrow p))\\
	L_2 &: \top\vdash((p\Rightarrow (q\Rightarrow r))\Rightarrow((p\Rightarrow q)\Rightarrow(p\Rightarrow r)))\\
	L_3 &: \top\vdash(((\neg p)\Rightarrow(\neg q))\Rightarrow(q\Rightarrow p))\\
	cong &: \top\vdash (p\Rightarrow q)\rightarrow \top\vdash (q\Rightarrow p)\rightarrow p\equiv q
\end{align*}
We can easily prove $\_\$\_$ and $L_1$, for the other proofs see Code \ref{src:nd_ham}.
\begin{align*}
	\_\$\_\ imp\psi\ pf\varphi&= app\ imp\psi\ [\ pair\ tt\ pf\varphi\ ]\\
	L_1 &= lam(lam(q\ [\ p\ ]))
\end{align*}

How can we specify a Hamilton-homomorphism from a categorical natural deduction homomorphism? Based on the previously discussed specification we can see that the $\llbracket\_\rrbracket$, $pv$, $\iota$ and $\_\Rightarrow\_$ fields of the categorical natural deduction homomorphism can be used without any changes. Only the $\llbracket\_\rrbracket_p$ and $\neg$ fields have to be proven. For the proof of $\llbracket\_\rrbracket_p$ see Code \ref{src:eqhamnd}. Proving $\neg$ happens similarly to the proofs so far. We have to prove that $\llbracket\varphi\Rightarrow^X\bot^X\rrbracket\equiv \llbracket\varphi \rrbracket\Rightarrow^Y\bot^Y$.
\begin{center}
	\AxiomC{}
	\LeftLabel{$Hom.\_\Rightarrow\_$}
	\UnaryInfC{$\llbracket\varphi\Rightarrow^X\bot^X\rrbracket\equiv \llbracket\varphi \rrbracket\Rightarrow^Y\llbracket\bot^X \rrbracket$}
	\AxiomC{$\_\Rightarrow^Y\_$}
	\AxiomC{}
	\LeftLabel{$Hom.\bot$}
	\UnaryInfC{$\llbracket\bot^X \rrbracket\equiv\bot^Y$}
	\LeftLabel{ap2snd}
	\BinaryInfC{$\llbracket\varphi \rrbracket\Rightarrow^Y\llbracket\bot^X \rrbracket\equiv \llbracket\varphi \rrbracket\Rightarrow^Y\bot^Y$}
	\LeftLabel{$\_\sq\_$}
	\BinaryInfC{$\llbracket\varphi\Rightarrow^X\bot^X\rrbracket\equiv \llbracket\varphi \rrbracket\Rightarrow^Y\bot^Y$}
	\DisplayProof
\end{center}

We defined a functor from the Hamilton to the natural deduction category and vice versa. Now we have to prove that the consecutive application of these functors yields an isomorphism.

The $f^\beta$ field of the equivalence requires an isomorphism between a natural deduction algebra and its image after applying $f\circ f^{-1}$ functor. $\llbracket\_\rrbracket$, $pv$, $\iota$, $\_\Rightarrow\_$ fields of the algebras are identical since the functors do not modify them. The remaining $\llbracket\_\rrbracket_p$, $\top$,$\_\wedge\_$,$\_\vee\_$ and $\bot$ fields have to be proven.
\begin{align*}
	\vdash^h\equiv\vdash&:\top\vdash(\Gamma\Rightarrow\varphi)\rightarrow \Gamma\vdash\varphi\\
	\vdash\equiv\vdash^h&: \Gamma\vdash\varphi\rightarrow\top\vdash(\Gamma\Rightarrow\varphi)\\
	\top^h\equiv\top&:(pv\  \iota)\Rightarrow(pv\ \iota)\equiv\top\\
	\wedge^h\equiv\wedge&:(\varphi\Rightarrow(\psi\Rightarrow\bot))\Rightarrow\bot\equiv\varphi\wedge\psi\\
	\vee^h\equiv\vee&:(\varphi\Rightarrow\bot)\Rightarrow\psi\equiv\varphi\vee\psi\\
	\bot^h\equiv\bot&:((pv\  \iota)\Rightarrow(pv\ \iota))\Rightarrow\bot\equiv\bot
\end{align*}

The first three has a short proof, for the others see Code \ref{src:nd_ham}.
\begin{align*}
	\vdash^h\equiv\vdash\ pf &=app\ pf\ [\ pair\ tt\ id\ ]&
	\vdash\equiv\vdash^h\ pf&=lam(pf\ [\ q\ ])\\
	\top^h\equiv\top&= cong\ tt\ (lam\ q)
\end{align*}

The $f^\eta$ field of the equivalence requires that a Hamilton algebra shall be isomorphic to the algebra given by $f^{-1}\circ f$ functor on the Hamilton category. $Form$, $pv$, $\iota$ and $\_\Rightarrow\_$ are again trivial to prove since the functors have no effect on them. Only the $\vdash$ and $\neg$ fields of the algebras have to be considered.
\begin{align*}
	\vdash^{nd}\equiv\vdash&:\vdash(((pv\  \iota)\Rightarrow(pv\ \iota))\Rightarrow\varphi)\rightarrow\vdash\varphi\\
	\vdash\equiv\vdash^{nd}&: \vdash\varphi\rightarrow\vdash(((pv\  \iota)\Rightarrow(pv\ \iota))\Rightarrow\varphi)\\
	\neg^{nd}\equiv\neg&:(\varphi\Rightarrow\neg((pv\  \iota)\Rightarrow(pv\ \iota)))\equiv\neg\varphi
\end{align*}
The first two are easy to prove, but for $\neg^{nd}\equiv\neg$ see Code \ref{src:ham_nd}.
\begin{align*}
	\vdash^{nd}\equiv\vdash&=\_\$\ id&
	\vdash\equiv\vdash^{nd}&=L_1\ \$\_
\end{align*}

In this way, we gave a functor from Hamilton category to natural deduction category and one in the opposite direction. With proving that their compositions yield isomorphisms, we are ready to say that the Hamilton and natural deduction categories are equivalent.