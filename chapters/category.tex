\chapter{Equivalence between propositional calculi}
\label{ch:category}

After summarizing all the different systems, we would like to somehow create a connection between them. At first glance, all four representations seem to be significantly different. Category theory helps us creating a framework in which we can formally define and prove the equivalence between these systems.

\section{Categories}

What is a category? Categories are algebraic structures. One way to think about them are graphs. A graph has nodes, edges and paths. Categories have objects and morphisms accordingly. Objects and morphisms are elements of classes, where each morphism is indexed by a source object and a target object. As paths are defined as list of consecutive edges in graphs, we can also define analogously the composition of morphisms. This composition shall be associative and shall have an identity element. Formally we can define categories the following way (for implementation see Code \ref{src:category})
\begin{align*}
	obj &: Set\\
	morph &: obj \rightarrow obj \rightarrow Set\\
	id &: morph\ X\ X\\
	\_\circ\_ &: morph\ Y\ Z \rightarrow morph\ X\ Y \rightarrow morph\ X\ Z.
\end{align*}
$id$ is the identity morphism of an object, therefore it is a right and left identity as well.
\begin{center}
	\AxiomC{}
	\LeftLabel{$id^r$}
	\UnaryInfC{$f\circ id\approx f$}
	\DisplayProof
	\quad
	\AxiomC{}
	\LeftLabel{$id^l$}
	\UnaryInfC{$id\circ f\approx f$}
	\DisplayProof
\end{center}
$\_\circ\_$ is the composition of morphisms. It shall be associative.
\begin{center}
	\AxiomC{}
	\LeftLabel{$assoc$}
	\UnaryInfC{$(f\circ g)\circ h\approx f\circ(g\circ h)$}
	\DisplayProof
\end{center}

We need the notion of isomorphism to describe the equivalence of two categories. Two objects $X$ and $Y$ of a category are said to be isomorphic if there exists a morphism from $X$ to $Y$, a counter morphism from $Y$ to $X$ and the composition of these morphisms are the identity morphism independently from the order of the composition. This can be formalized in the following way (for implementation see Code \ref{src:cat_iso})
\begin{align*}
	f &: morph\ X\ Y \\
	f^{-1} &: morph\ Y\ X\\
	f^\beta &: f \circ f^{-1} \equiv id \\
	f^\eta &: f^{-1} \circ f \equiv id.
\end{align*}
An isomorphism can be interpreted as a travel from object $X$ to $Y$ by using $f$ and a way back by using $f^{-1}$. This is depicted at Figure \ref{fig:iso}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.33\textwidth]{iso}
	\caption{Isomorphism between objects $X$ and $Y$ in category $A$}
	\label{fig:iso}
\end{figure}

We can define the composition of isomorphisms by composing the corresponding morphisms and proving $f^\beta$ and $f^\eta$. This way the result is also an isomorphism (see Code \ref{src:cat_isocomp}).

We almost defined everything we need to prove the equivalence of the categories. Another notion that we need are functors. Functors can be interpreted as morphisms between categories. A functor from category $A$ to category $B$ maps each object of $A$ to an object of $B$. It maps the morphisms as well. A functor maps $A$'s identity onto $B$'s identity, and the composition of two morphisms in $A$ onto the composition of their images in $B$. This can be formalized in the following way (see Code \ref{src:cat_func})
\begin{align*}
	\llbracket\_\rrbracket_o &: obj^A \rightarrow obj^B\\
	\llbracket\_\rrbracket_m &: morph^A\ X\ Y \rightarrow morph^B\ \llbracket X\rrbracket_o\ \llbracket Y\rrbracket_o\\
	id &:\llbracket id^A\rrbracket_m\equiv id^B\\
	\_\circ\_&: \llbracket f\circ^A g\rrbracket_m\equiv\llbracket f\rrbracket_m\circ^B\llbracket g\rrbracket_m.
\end{align*}
A visual sketch can be seen at Figure \ref{fig:func}.
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{func}
	\caption{Functor from category $A$ to category $B$}
	\label{fig:func}
\end{figure}

Similarly to the isomorphisms, the composition of functors can be defined (see Code \ref{src:cat_funccomp}). Also it can be proved that functors respect isomorphism, so if $X$ and $Y$ are isomorphic objects, then after applying a functor, we find that their images are also isomorphic (see Code \ref{src:cat_api}). 

With the composition of functors we are able to describe the equivalence of categories. Similarly to the isomorphism of two objects, we can define the equivalence of two categories. Two categories are said to be equivalent if there is a functor from one to the other and vice versa and each object is isomorphic to its image under the composition of these two functors. Equivalence can be formalized in the following way (see Code \ref{src:cat_eq})
\begin{align*}
	f &: Functor\ A\ B \\
	f^{-1} &: Functor\ B\ A\\
	f^\beta &: Iso\ (f \circ f^{-1}\ Y)\ Y \\
	f^\eta &: Iso\ (f^{-1} \circ f\ X)\ X.
\end{align*}

$f^\beta$ can be interpreted as we start from an $Y$ object of category $B$. Then we move to category $A$ by using $f^{-1}$ as a functor from $B$ to category $A$. Then using $f$ we can go back to category $B$, but this target object is not necessarily the source object $Y$, however it will be isomorphic to $Y$. By using the $f^\beta$ isomorphism's $f$, we can go back to $Y$. This whole process is depicted at Figure \ref{fig:eq_fbeta}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{eq_fbeta}
	\caption{$f^\beta$ of an equivalence between category $A$ and $B$}
	\label{fig:eq_fbeta}
\end{figure}

$f^\eta$ can be interpreted similarly, but this time we start from an $X$ object of category $A$. This can be mapped to category $B$ with using $f$ as a functor from $A$ to category $B$. Then using $f^{-1}$ we can go back to category $A$, but this target object is not necessarily the source object $X$, however it will be isomorphic to $X$. By using the $f^\eta$ isomorphism's $f$ morphism, we can go back to $X$. This whole process is depicted at Figure \ref{fig:eq_feta}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{eq_feta}
	\caption{$f^\eta$ of an equivalence between category $A$ and $B$}
	\label{fig:eq_feta}
\end{figure}

Similarly to isomorphisms and functors, the composition of equivalences can be defined. If category $A$ is equivalent to category $B$ and category $B$ is equivalent to category $C$, then category $A$ is equivalent to category $C$ (see Code \ref{src:cat_eqcomp}).

Now we arrived at the point where we are able to formally compare the four descriptions with the tool set provided by category theory. The algebras can be interpreted as object of a category, transforming these categories into each other can be achieved with functors and the equivalence of the categories can be proved. By providing pairwise equivalence we can easily prove the equivalence between all of them, since equivalence is transitive.